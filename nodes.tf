module "nodegroup_instance_role" {
  source          = "./modules/iam/instance_role"
  create          = var.create
  resource_create = var.task
  name            = "eks-nodegroup-role"
  tags            = var.tags
}

module "eks_nodegroup_role_policy_attachment" {
  source          = "./modules/iam/policy_attachment"
  create          = var.create
  resource_create = var.task
  roles           = module.nodegroup_instance_role.role_name
  policy_arn      = ["arn:${data.aws_partition.task_current.partition}:iam::aws:policy/AmazonEKSWorkerNodePolicy", "arn:${data.aws_partition.task_current.partition}:iam::aws:policy/AmazonEKS_CNI_Policy", "arn:${data.aws_partition.task_current.partition}:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"]
}

module "eks_nodegroup" {
  source = "./modules/node_group"
  cluster_name =  module.eks_cluster.eks_cluster_id
  create          = var.create
  create_nodes      = var.task
  module_depends_on = [module.eks_nodegroup_role_policy_attachment]
  node_role_arn = module.nodegroup_instance_role.role_arn
  subnet_ids       = [module.task_sub_private_devops.private_subnet_id, module.task_sub_public_devops.public_subnet_id]
  desired_size = 1
  min_size     = 1
  max_size     = 1
  tags = merge(
    {
      "kubernetes.io/cluster/eks" = "owned"
      Environment                 = "test"
    },
    var.tags
  )
  kubernetes_labels = {
    lifecycle = "OnDemand"
    #az        = "eu-west-1a"
  }
}