variable "resource_create" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  default     = false
}

variable "create" {
  default     = false
  description = "Master control variable if VPC should be created (it affects almost all resources)"
}

variable "app" {
  type        = string
  default     = "golang"
  description = "Application name"
}

variable "env" {
  type        = string
  description = "Enviroment name"
  default     = "dev"
}

variable "resource_type" {
  type        = string
  description = "Resource type(eks, ecs, ec2, lambda)"
  default     = "ec2"
}

variable "resourcename" {
  type        = string
  description = "Resource name(deveks, devec2, devecs, devlambda)"
  default     = "myec2"
}

variable "namespace" {
  type        = string
  description = "Namespace"
  default     = "cluster"
}

variable "retention_in_days" {
  type        = number
  description = "Log retention in days"
  default     = 2
}

variable "encryption_key" {
  type        = list
  default     = []
  description = "IAM service role for the build project"
}

variable "stream_names" {
  default     = []
  type        = list
  description = "Names of streams"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit', 'XYZ')`"
}