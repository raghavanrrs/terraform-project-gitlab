resource "aws_cloudwatch_log_group" "default" {
  count = var.create && var.resource_create ? 1 : 0
  #name              = format("%s-%s-%s-%s", "log-group", var.app, var.env, count.index + 1)
  name              = format("%s/%s/%s/%s", "custom-namespace", var.resource_type, var.resourcename, var.namespace)
  retention_in_days = var.retention_in_days
  tags = merge(
    {
      "Name" = format("%s-%s-%s-%s", "log-group", var.app, var.env, count.index + 1)
    },
    var.tags
  )
  kms_key_id = var.encryption_key != "" ? element(concat(var.encryption_key, list("")), count.index) : null
}

resource "aws_cloudwatch_log_stream" "default" {
  count          = var.create && var.resource_create && length(var.stream_names) > 0 ? length(var.stream_names) : 0
  name           = element(var.stream_names, count.index)
  log_group_name = element(aws_cloudwatch_log_group.default.*.name, 0)
}