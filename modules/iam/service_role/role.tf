resource "aws_iam_role" "service_role" {
  count                = var.create && var.resource_create ? 1 : 0
  name                 = format("%s-%s", var.name, count.index + 1)
  path                 = var.path
  assume_role_policy   = var.assume_role_policy
  description          = var.description
  max_session_duration = var.max_session_duration
  permissions_boundary = var.permissions_boundary
  tags                 = var.tags
}