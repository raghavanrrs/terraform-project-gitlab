variable "resource_create" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  type        = bool
  default     = false
}

variable "create" {
  default     = false
  type        = bool
  description = "Master control variable if VPC should be created (it affects almost all resources)"
}


variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = ""
}

variable "tags" {
  type    = map
  default = {}
}

variable "path" {
  type        = string
  description = "Path for role"
  default     = "/service-role/"
}

variable "description" {
  type        = string
  description = "Description for role"
  default     = "IAM role"
}

variable "max_session_duration" {
  type        = number
  default     = 3600
  description = "The maximum session duration (in seconds) that you want to set for the specified role. If you do not specify a value for this setting, the default maximum of one hour is applied. This setting can have a value from 1 hour to 12 hours."
}

variable "permissions_boundary" {
  type        = string
  default     = ""
  description = "The ARN of the policy that is used to set the permissions boundary for the role."
}

variable "assume_role_policy" {
  type        = string
  description = "Assume role policy document for IAM role(defaults to EC2)"
  default     = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}