variable "maintenance_mode" {
  type    = bool
  default = false
}