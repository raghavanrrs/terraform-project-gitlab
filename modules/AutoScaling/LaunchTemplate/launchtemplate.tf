resource "aws_launch_template" "cluster" {
  count           = var.create ? 1 : 0
  image_id               = var.image_id
  instance_type          = var.instance_type
  name                   = "eks-launch-template-test"
  update_default_version = true

  key_name = "eks-test"

  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      volume_size = 20
    }
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name                        = "eks-node-group-instance-name"
      "kubernetes.io/cluster/eks" = "owned"
    }
  }

  user_data = base64encode(templatefile("userdata.tpl", { CLUSTER_NAME = aws_eks_cluster.cluster.name, B64_CLUSTER_CA = aws_eks_cluster.cluster.certificate_authority[0].data, API_SERVER_URL = aws_eks_cluster.cluster.endpoint }))
}