locals {
  deployment_groups = [
    {
      name_prefix = "green"
    },
    {
      name_prefix = "blue"
    },
  ]
}

resource "aws_lb_target_group" "this" {
  count       = var.create && var.resource_create ? length(local.target_groups) : 0
  name        = lookup(local.deployment_groups[count.index], "name", null)
  name_prefix = lookup(local.deployment_groups[count.index], "name_prefix", null)

  port        = lookup(var.target_groups[count.index], "backend_port", null)
  protocol    = lookup(var.target_groups[count.index], "backend_protocol", null) != null ? upper(lookup(var.target_groups[count.index], "backend_protocol")) : null
  target_type = lookup(var.target_groups[count.index], "target_type", null)
  vpc_id      = var.vpc_id

  deregistration_delay               = lookup(var.target_groups[count.index], "deregistration_delay", null)
  slow_start                         = lookup(var.target_groups[count.index], "slow_start", null)
  proxy_protocol_v2                  = lookup(var.target_groups[count.index], "proxy_protocol_v2", false)
  lambda_multi_value_headers_enabled = lookup(var.target_groups[count.index], "lambda_multi_value_headers_enabled", false)
  load_balancing_algorithm_type      = lookup(var.target_groups[count.index], "load_balancing_algorithm_type", null)

  dynamic "health_check" {
    for_each = length(keys(lookup(var.target_groups[count.index], "health_check", {}))) == 0 ? [] : [lookup(var.target_groups[count.index], "health_check", {})]

    content {
      enabled             = lookup(health_check.value, "enabled", null)
      interval            = lookup(health_check.value, "interval", null)
      path                = lookup(health_check.value, "path", null)
      port                = lookup(health_check.value, "port", null)
      healthy_threshold   = lookup(health_check.value, "healthy_threshold", null)
      unhealthy_threshold = lookup(health_check.value, "unhealthy_threshold", null)
      timeout             = lookup(health_check.value, "timeout", null)
      protocol            = lookup(health_check.value, "protocol", null)
      matcher             = lookup(health_check.value, "matcher", null)
    }
  }

  dynamic "stickiness" {
    for_each = length(keys(lookup(var.target_groups[count.index], "stickiness", {}))) == 0 ? [] : [lookup(var.target_groups[count.index], "stickiness", {})]

    content {
      enabled         = lookup(stickiness.value, "enabled", null)
      cookie_duration = lookup(stickiness.value, "cookie_duration", null)
      type            = lookup(stickiness.value, "type", null)
    }
  }

  tags = merge(
    var.tags,
    var.target_group_tags,
    lookup(var.target_groups[count.index], "tags", {}),
    {
      "Name" = lookup(local.deployment_groups[count.index], "name", lookup(local.deployment_groups[count.index], "name_prefix", ""))
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}

# resource "aws_lb_target_group" "targergroup" {
#   count = var.create && var.resource_create ? length(local.target_groups) : 0

#   name        = lookup(var.target_groups[count.index], "name", null)
#   name_prefix = lookup(local.target_groups[count.index], "name_prefix", null)

#   vpc_id      = var.vpc_id
#   port        = lookup(var.target_groups[count.index], "backend_port", null)
#   protocol    = lookup(var.target_groups[count.index], "backend_protocol", null) != null ? upper(lookup(var.target_groups[count.index], "backend_protocol")) : null
#   target_type = lookup(var.target_groups[count.index], "target_type", null)

#   deregistration_delay               = lookup(var.target_groups[count.index], "deregistration_delay", null)
#   slow_start                         = lookup(var.target_groups[count.index], "slow_start", null)
#   proxy_protocol_v2                  = lookup(var.target_groups[count.index], "proxy_protocol_v2", false)
#   lambda_multi_value_headers_enabled = lookup(var.target_groups[count.index], "lambda_multi_value_headers_enabled", false)
#   load_balancing_algorithm_type      = lookup(var.target_groups[count.index], "load_balancing_algorithm_type", null)

#   dynamic "health_check" {
#     for_each = length(keys(lookup(var.target_groups[count.index], "health_check", {}))) == 0 ? [] : [lookup(var.target_groups[count.index], "health_check", {})]

#     content {
#       enabled             = lookup(health_check.value, "enabled", null)
#       interval            = lookup(health_check.value, "interval", null)
#       path                = lookup(health_check.value, "path", null)
#       port                = lookup(health_check.value, "port", null)
#       healthy_threshold   = lookup(health_check.value, "healthy_threshold", null)
#       unhealthy_threshold = lookup(health_check.value, "unhealthy_threshold", null)
#       timeout             = lookup(health_check.value, "timeout", null)
#       protocol            = lookup(health_check.value, "protocol", null)
#       matcher             = lookup(health_check.value, "matcher", null)
#     }
#   }

#   dynamic "stickiness" {
#     for_each = length(keys(lookup(var.target_groups[count.index], "stickiness", {}))) == 0 ? [] : [lookup(var.target_groups[count.index], "stickiness", {})]

#     content {
#       enabled         = lookup(stickiness.value, "enabled", null)
#       cookie_duration = lookup(stickiness.value, "cookie_duration", null)
#       type            = lookup(stickiness.value, "type", null)
#     }
#   }

#   tags = merge(
#     var.tags,
#     var.target_group_tags,
#     lookup(var.target_groups[count.index], "tags", {}),
#     {
#       "Name" = lookup(var.target_groups[count.index], "name", lookup(var.target_groups[count.index], "name_prefix", ""))
#     },
#   )


#   lifecycle {
#     create_before_destroy = true
#   }
# }