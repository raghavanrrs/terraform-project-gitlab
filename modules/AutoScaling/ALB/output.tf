output "alb_lb_id" {
  description = "The ID and ARN of the load balancer we created."
  value       = concat(aws_lb.alb.*.id, [""])[0]
}

output "alb_lb_arn" {
  description = "The ID and ARN of the load balancer we created."
  value       = concat(aws_lb.alb.*.arn, [""])[0]
}

output "alb_lb_dns_name" {
  description = "The DNS name of the load balancer."
  value       = concat(aws_lb.alb.*.dns_name, [""])[0]
}

output "alb_lb_arn_suffix" {
  description = "ARN suffix of our load balancer - can be used with CloudWatch."
  value       = concat(aws_lb.alb.*.arn_suffix, [""])[0]
}

output "alb_lb_zone_id" {
  description = "The zone_id of the load balancer to assist with creating DNS records."
  value       = concat(aws_lb.alb.*.zone_id, [""])[0]
}
