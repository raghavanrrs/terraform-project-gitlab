output "bucket_id" {
  description = "The name of the bucket."
  value       = aws_s3_bucket.s3.*.id
}

output "bucket_arn" {
  description = "he ARN of the bucket. Will be of format arn:aws:s3:::bucketname."
  value       = aws_s3_bucket.s3.*.arn
}

output "bucket_domain_name" {
  description = "The bucket domain name. Will be of format bucketname.s3.amazonaws.com."
  value       = aws_s3_bucket.s3.*.bucket_domain_name
}

output "bucket_regional_domain_name" {
  description = "The bucket region-specific domain name."
  value       = aws_s3_bucket.s3.*.bucket_regional_domain_name
}

output "hosted_zone_id" {
  description = "The Route 53 Hosted Zone ID for this bucket's region."
  value       = aws_s3_bucket.s3.*.hosted_zone_id
}

# output "bucket_region" {
#   description = "The AWS region this bucket resides in."
#   value = aws_s3_bucket.s3.*.bucket_region
# }

output "website_endpoint" {
  description = "The website endpoint, if the bucket is configured with a website. If not, this will be an empty string."
  value       = aws_s3_bucket.s3.*.website_endpoint
}

output "website_domain" {
  description = "The domain of the website endpoint, if the bucket is configured with a website. If not, this will be an empty string. This is used to create Route 53 alias records."
  value       = aws_s3_bucket.s3.*.website_domain
}