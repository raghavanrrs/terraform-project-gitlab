output "nodegroup_cluster_resources" {
  description = "List of objects containing information about underlying resources."
  value       = element(concat(aws_eks_node_group.node_group.*.resources, list("")), 0)
}

output "nodegroup_cluster_arn" {
  description = "Amazon Resource Name (ARN) of the EKS Node Group."
  value       = element(concat(aws_eks_node_group.node_group.*.arn, list("")), 0)
}

output "nodegroup_cluster_id" {
  description = "EKS Cluster name and EKS Node Group name separated by a colon (:)."
  value       = element(concat(aws_eks_node_group.node_group.*.id, list("")), 0)
}

output "nodegroup_cluster_status" {
  description = "Status of the EKS Node Group."
  value       = element(concat(aws_eks_node_group.node_group.*.status, list("")), 0)
}