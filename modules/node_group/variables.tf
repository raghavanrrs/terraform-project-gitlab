variable "create_nodes" {
  description = "Controls if EKS should be created"
  default     = false
}

variable "create" {
  default     = false
  description = "Master control variable if VPC should be created (it affects almost all resources)"
}

variable "cluster_name" {
  type        = string
  default     = ""
  description = "EKS Cluster name"
}

variable "node_group_name" {
  type        = string
  default     = "small"
  description = "EKS Cluster Node-Group name"
}

variable "node_role_arn" {
  type        = list
  default     = []
  description = "IAM service role for the node-group instances"
}

variable "subnet_ids" {
  description = "A list of subnet IDs to launch resources in"
  type        = list
  default = []
}

variable "kubernetes_labels" {
  type        = map(string)
  description = "Key-value mapping of Kubernetes labels. Only labels that are applied with the EKS API are managed by this argument. Other Kubernetes labels applied to the EKS Node Group will not be managed"
  default     = {}
}

variable "desired_size" {
  type        = number
  description = "Desired number of worker nodes"
  default = 0
}

variable "max_size" {
  type        = number
  description = "Maximum number of worker nodes"
  default = 0
}

variable "min_size" {
  type        = number
  description = "Minimum number of worker nodes"
  default = 0
}

variable "ec2_ssh_key" {
  type        = string
  description = "SSH key name that should be used to access the worker nodes"
  default     = null
}

variable "source_security_group_ids" {
  type        = list(string)
  default     = []
  description = "Set of EC2 Security Group IDs to allow SSH access (port 22) from on the worker nodes. If you specify `ec2_ssh_key`, but do not specify this configuration when you create an EKS Node Group, port 22 on the worker nodes is opened to the Internet (0.0.0.0/0)"
}

variable "launch_template" {
  type        = map(string)
  description = "Configuration block with Launch Template settings. `name`, `id` and `version` parameters are available."
  default     = {}
}

variable "module_depends_on" {
  type        = list
  default     = []
  description = "Dependencies resources"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit', 'XYZ')`"
}


