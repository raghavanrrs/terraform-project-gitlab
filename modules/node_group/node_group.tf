resource "aws_eks_node_group" "node_group" {
  count           = var.create && var.create_nodes ? 1 : 0
  cluster_name    = var.cluster_name
  node_group_name = format("%s-%s%s", var.node_group_name, "node-group", element(concat(random_string.random_id.*.result, list("")), count.index))
  node_role_arn   = element(concat(var.node_role_arn, list("")), count.index)
  subnet_ids      = flatten([var.subnet_ids])
  force_update_version = true
  labels         = var.kubernetes_labels

  scaling_config {
    desired_size = var.desired_size
    max_size     = var.max_size
    min_size     = var.min_size
  }
  
  dynamic "remote_access" {
    for_each = var.ec2_ssh_key != null && var.ec2_ssh_key != "" ? ["true"] : []
    content {
      ec2_ssh_key               = var.ec2_ssh_key
      source_security_group_ids = var.source_security_group_ids
    }
  }

  dynamic "launch_template" {
    for_each = length(var.launch_template) == 0 ? [] : [var.launch_template]
    content {
      id      = lookup(launch_template.value, "id", null)
      name    = lookup(launch_template.value, "name", null)
      version = lookup(launch_template.value, "version")
    }
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [scaling_config.0.desired_size]
  }

  tags = merge(
    {
      "Name" = format("%s-%s%s", var.node_group_name, "node-group", element(concat(random_string.random_id.*.result, list("")), count.index))
    },
    var.tags
  )

  depends_on = [null_resource.node_depends_on]
}

resource "null_resource" "node_depends_on" {
  count = var.create_nodes ? 1 : 0
  triggers = {
    value = length(var.module_depends_on)
  }
}

resource "random_string" "random_id" {
  count            = var.create && var.create_nodes ? 1 : 0
  length           = 5
  special          = false
  override_special = "/_+=.@-"
}