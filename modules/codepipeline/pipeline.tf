# locals {
#   build_actions = fileexists("./build_actions.tmpl") ? jsondecode(templatefile("./build_actions.tmpl", { client = var.client, stage = var.stage })) : null
# }

resource "aws_codepipeline" "pipeline" {
  count    = var.create && var.resource_create ? 1 : 0
  name     = format("%s-%s-%s-%s", "codepipeline", var.app, var.env, count.index + 1)
  role_arn = element(concat(var.service_role, list("")), count.index)

  tags = merge(
    {
      "Name" = format("%s-%s-%s-%s", "codepipeline", var.app, var.env, count.index + 1)
    },
    var.tags
  )

  artifact_store {
    location = aws_s3_bucket.this.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "ThirdParty"
      provider         = "GitHub"
      version          = "1"
      output_artifacts = ["SourceArtifact"]

      configuration = {
        OAuthToken = var.github_token
        # In GitHub the path to a repo is owner/repo_name
        Owner  = var.github_repo_owner
        Repo   = var.github_repo_name
        Branch = var.github_repo_branch
      }
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      version          = "1"
      input_artifacts  = ["SourceArtifact"]
      output_artifacts = ["BuildArtifact"]

      configuration = {
        ProjectName = aws_codebuild_project.this.name
      }
    }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeployToECS"
      version         = "1"
      input_artifacts = ["BuildArtifact"]

      configuration = {
        ApplicationName                = aws_codedeploy_app.this.name
        DeploymentGroupName            = aws_codedeploy_deployment_group.this.deployment_group_name
        TaskDefinitionTemplateArtifact = "BuildArtifact"
        AppSpecTemplateArtifact        = "BuildArtifact"
      }
    }
  }
}