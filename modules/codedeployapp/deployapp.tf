resource "aws_codedeploy_app" "cdapp" {
  count            = var.create && var.resource_create ? 1 : 0
  compute_platform = var.compute_platform
  name             = format("%s-%s-%s-%s", "codedeployapp", var.app, var.env, count.index + 1)
}