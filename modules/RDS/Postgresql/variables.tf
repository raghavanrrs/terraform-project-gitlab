variable "alloc_storage" {
  default = ""
}

variable "storage_type" {
  default = ""
}

variable "engine" {
  default = ""
}

variable "engine_version" {
  default = ""
}

variable "instance_class" {
  default = ""
}

variable "subnet_group" {
  type    = list
  default = []
}

variable "public" {
  default = ""
}

variable "security_group" {
  type    = list
  default = []
}

variable "AZ" {
  default = ""
}

variable "identifier" {
  default = ""
}

variable "dbname" {
  default = ""
}

variable "dbuser" {
  default = ""
}

variable "dbpassword" {
  default = ""
}

variable "parameter_group" {
  default = ""
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted, using the value from final_snapshot_identifier"
  type        = bool
  default     = true
}
variable "kms_arn" {
  default = ""
}

variable "resource_create" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  default     = false
}

variable "create" {
  default     = false
  description = "Master control variable if VPC should be created (it affects almost all resources)"
}

variable "tags" {
  type    = map
  default = {}
}

variable "multi_az" {
  description = "multi az deployment"
  default     = false
}

variable "storage_encrypted" {
  default = false
}

variable "databasename" {
  default = "CustomerData"
}

variable "iam_database_authentication_enabled" {
  description = "IAM DB Auth"
  default     = false
}

variable "monitoring_interval" {
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance. To disable collecting Enhanced Monitoring metrics, specify 0. The default is 0. Valid Values: 0, 1, 5, 10, 15, 30, 60."
  type        = number
  default     = 0
}

variable "monitoring_role_arn" {
  description = "The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs. Must be specified if monitoring_interval is non-zero."
  type        = string
  default     = ""
}

variable "monitoring_role_name" {
  description = "Name of the IAM role which will be created when create_monitoring_role is enabled."
  type        = string
  default     = "rds-monitoring-role"
}

variable "create_monitoring_role" {
  description = "Create IAM role with a defined name that permits RDS to send enhanced monitoring metrics to CloudWatch Logs."
  type        = bool
  default     = false
}

variable "copy_tags_to_snapshot" {
  description = "On delete, copy all Instance tags to the final snapshot (if final_snapshot_identifier is specified)"
  type        = bool
  default     = false
}
