resource aws_db_instance "db" {
  count                               = var.create && var.resource_create ? 1 : 0
  allocated_storage                   = var.alloc_storage
  storage_type                        = var.storage_type
  engine                              = var.engine
  engine_version                      = var.engine_version
  instance_class                      = var.instance_class
  db_subnet_group_name                = element(concat(var.subnet_group, list("")), count.index)
  enabled_cloudwatch_logs_exports     = ["postgresql", "upgrade"]
  publicly_accessible                 = var.public
  vpc_security_group_ids              = var.security_group
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  copy_tags_to_snapshot               = var.copy_tags_to_snapshot
  storage_encrypted                   = var.instance_class != "db.t2.micro" && var.storage_encrypted ? var.storage_encrypted : false
  multi_az                            = var.multi_az
  kms_key_id                          = var.instance_class != "db.t2.micro" && var.storage_encrypted ? var.kms_arn : null
  availability_zone                   = var.multi_az ? null : var.AZ
  identifier                          = lower(var.identifier)
  name                                = var.dbname
  username                            = var.dbuser
  password                            = var.dbpassword
  parameter_group_name                = element(concat(aws_db_parameter_group.default.*.id, list("")), count.index)
  skip_final_snapshot                 = var.skip_final_snapshot
  monitoring_interval                 = var.monitoring_interval
  monitoring_role_arn                 = var.monitoring_interval > 0 ? coalesce(var.monitoring_role_arn, join(", ", aws_iam_role.enhanced_monitoring.*.arn), null) : null
  tags = merge(
    {
      "Name" = format("%s-%s%s", var.dbname, "0", count.index + 1)
    },
    var.tags
  )
}

data "aws_iam_policy_document" "enhanced_monitoring" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "enhanced_monitoring" {
  count = var.create && var.resource_create && var.create_monitoring_role ? 1 : 0

  name               = var.monitoring_role_name
  assume_role_policy = data.aws_iam_policy_document.enhanced_monitoring.json

  tags = merge(
    {
      "Name" = format("%s", var.monitoring_role_name)
    },
    var.tags,
  )
}

resource "aws_iam_role_policy_attachment" "enhanced_monitoring" {
  count = var.create && var.resource_create && var.create_monitoring_role ? 1 : 0

  role       = aws_iam_role.enhanced_monitoring[0].name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

resource "aws_db_parameter_group" "default" {
  count  = var.create && var.resource_create ? 1 : 0
  name   = "rds-pg"
  family = var.parameter_group

  parameter {
    name  = "log_statement"
    value = "all"
  }

}
