output "rds_endpoint" {
  value = aws_db_instance.db.*.endpoint
}

output "rds_arn" {
  value = aws_db_instance.db.*.arn
}

output "rds_address" {
  value = aws_db_instance.db.*.address
}

output "rds_id" {
  value = aws_db_instance.db.*.id
}

output "rds_db_name" {
  value = aws_db_instance.db.*.name
}

output "rds_resource_id" {
  value = aws_db_instance.db.*.resource_id
}

output "rds_username" {
  value = aws_db_instance.db.*.username
}