variable "app" {
  type        = string
  default     = "golang"
  description = "Application name"
}

variable "tag_prefix_list" {
  type        = list(string)
  description = "List of image tag prefixes on which to take action with lifecycle policy."
  default = ["release"]
}

variable "only_pull_accounts" {
  default     = []
  type        = list(string)
  description = "AWS accounts which pull only."
}

variable "push_and_pull_accounts" {
  default     = []
  type        = list(string)
  description = "AWS accounts which push and pull."
}

variable "max_untagged_image_count" {
  default     = 1
  type        = number
  description = "The maximum number of untagged images that you want to retain in repository."
}

variable "max_tagged_image_count" {
  default     = 30
  type        = number
  description = "The maximum number of tagged images that you want to retain in repository."
}

variable "scan_on_push" {
  default     = false
  type        = bool
  description = "Whether images should automatically be scanned on push or not."
}

variable "image_tag_mutability" {
  default     = "MUTABLE"
  type        = string
  description = "Whether images are allowed to overwrite existing tags."
}

variable "repo_policy" {
    type        = string
    default = ""
    description = "Repo Policy in JSON"
}

variable "attach_lifecycle_policy" {
  default     = false
  type        = bool
  description = "If true, an ECR lifecycle policy will be attached"
}

variable "lifecycle_policy" {
  default     = ""
  type        = string
  description = "Contents of the ECR lifecycle policy"
}

variable "kms_key" {
  default     = ""
  type        = string
  description = "KMS key for repo encryption"
}


variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit', 'XYZ')`"
}