resource "aws_s3_bucket_object" "files" {
  count         = var.create && var.resource_create ? 1 : 0
  bucket        = var.bucket_name
  key           = var.prefix != null ? var.file_name : format("%s/%s", var.prefix, var.file_name)
  source        = var.file_name
  acl           = "private"
  etag          = filemd5(basename(var.file_name))
  force_destroy = var.force_destroy
  #content_type = lookup(var.mime_types, split(".", each.value)[1])
  tags                   = var.tags
  server_side_encryption = var.encryption_method
}

resource "null_resource" "green_trigger" {
  count = var.create && var.resource_create ? 1 : 0
  triggers = {
    artifact_etag = element(aws_s3_bucket_object.files.*.etag, count.index)
    version_id    = element(aws_s3_bucket_object.files.*.version_id, count.index)
  }
}