variable "create_eks" {
  description = "Controls if EKS should be created"
  default     = false
}

variable "create" {
  default     = false
  description = "Master control variable if VPC should be created (it affects almost all resources)"
}

variable "app" {
  type        = string
  default     = "golang"
  description = "Application name"
}

variable "env" {
  type        = string
  description = "Enviroment name"
  default     = "dev"
}

variable "service_role" {
  type        = list
  default     = []
  description = "IAM service role for the build project"
}

variable "cluster_version" {
  type        = number
  description = "Cluster Version, Defaults to 1.17"
  default     = 1.17
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit', 'XYZ')`"
}

variable "cluster_security_group_id" {
  description = "If provided, the EKS cluster will be attached to this security group. If not given, a security group will be created with necessary ingres/egress to work with the workers and provide API access to your current IP/32."
  type        = list
  default     = []
}

variable "additonal_cluster_security_group" {
  description = "Additional SG to include to cluster"
  type        = list
  default     = []
}

variable "subnets" {
  description = "A list of subnets to place the EKS cluster and workers within."
  type        = list
}

variable "cluster_endpoint_private_access" {
  description = "Indicates whether or not the Amazon EKS private API server endpoint is enabled."
  default     = true
}

variable "cluster_endpoint_public_access" {
  description = "Indicates whether or not the Amazon EKS public API server endpoint is enabled."
  default     = true
}

variable "cluster_endpoint_public_access_cidrs" {
  description = "List of CIDR blocks. Indicates which CIDR blocks can access the Amazon EKS public API server endpoint when enabled."
  default     = ["0.0.0.0/0"]
  type        = list
}

variable "cluster_create_timeout" {
  description = "Timeout value when creating the EKS cluster."
  default     = "30m"
}

variable "cluster_delete_timeout" {
  description = "Timeout value when deleting the EKS cluster."
  default     = "30m"
}

variable "key_arn" {
  type        = list
  default     = []
  description = "KMS key for the build project"
}

variable "module_depends_on" {
  type        = list
  default     = []
  description = "Dependencies resources"
}

variable "cluster_name" {
  type = string
  description = "Clustername"
}

variable "region" {
  type = string
  description = "region name"
  default = "ap-southeast-1"
}

variable "node_role" {
  type        = list
  default     = []
  description = "IAM service role for nodes"
}