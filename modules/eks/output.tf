output "eks_cluster_id" {
  description = "The Cluster ID of the cluster."
  value       = element(concat(aws_eks_cluster.cluster.*.id, list("")), 0)
}

output "eks_cluster_arn" {
  description = "The Amazon Resource Name (ARN) of the cluster."
  value = element(concat(aws_eks_cluster.cluster.*.arn, list("")), 0)
}

output "eks_cluster_certificate_authority_data" {
  description = "Nested attribute containing certificate-authority-data for your cluster. This is the base64 encoded certificate data required to communicate with your cluster."
  value       = element(concat(aws_eks_cluster.cluster[*].certificate_authority[0].data, list("")), 0)
}

output "eks_cluster_endpoint" {
  description = "The endpoint for your EKS Kubernetes API."
  value       = element(concat(aws_eks_cluster.cluster.*.endpoint, list("")), 0)
}

output "eks_cluster_version" {
  description = "The Kubernetes server version for the EKS cluster."
  value       = element(concat(aws_eks_cluster.cluster[*].version, list("")), 0)
}

output "eks_cluster_name" {
  description = "The Kubernetes server name for the EKS cluster."
  value       = element(concat(aws_eks_cluster.cluster[*].name, list("")), 0)
}

output "eks_cluster_platform_version" {
  description = "The platform version for the cluster."
  value       = element(concat(aws_eks_cluster.cluster[*].platform_version, list("")), 0)
}

output "eks_cluster_status" {
  description = "The status of the EKS cluster. One of CREATING, ACTIVE, DELETING, FAILED."
  value       = element(concat(aws_eks_cluster.cluster[*].status, list("")), 0)
}

output "eks_cluster_vpc_config" {
  description = "VPC Config nested attributes."
  value       = element(concat(aws_eks_cluster.cluster[*].vpc_config, list("")), 0)
}


locals {
  config_map_aws_auth = <<CONFIGMAPAWSAUTH
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${element(concat(var.service_role, list("")), 0)}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
    - rolearn: ${element(concat(var.node_role, list("")), 0)}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
CONFIGMAPAWSAUTH

  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    server: ${aws_eks_cluster.cluster[0].endpoint}
    certificate-authority-data: ${aws_eks_cluster.cluster[0].certificate_authority[0].data}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${var.cluster_name}"
KUBECONFIG
}

output "config_map_aws_auth" {
  value = local.config_map_aws_auth
}

output "kubeconfig" {
  value = local.kubeconfig
}

output "monitoring_template" {
  value = data.null_data_source.policy[0].outputs["tmpl"]
}