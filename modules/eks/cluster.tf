# follow https://github.com/NassK/eks_medium/blob/master/
resource "aws_eks_cluster" "cluster" {
  count                     = var.create && var.create_eks ? 1 : 0
  name                      = var.cluster_name
  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  role_arn                  = element(concat(var.service_role, list("")), count.index)
  version                   = var.cluster_version
  tags                      = var.tags

  vpc_config {
    security_group_ids      = compact(concat(var.cluster_security_group_id, var.additonal_cluster_security_group))
    subnet_ids              = flatten([var.subnets])
    endpoint_private_access = var.cluster_endpoint_private_access
    endpoint_public_access  = var.cluster_endpoint_public_access
    public_access_cidrs     = var.cluster_endpoint_public_access_cidrs
  }

  timeouts {
    create = var.cluster_create_timeout
    delete = var.cluster_delete_timeout
  }

  encryption_config {
    provider {
      key_arn = element(concat(var.key_arn, list("")), count.index)
    }
    resources = ["secrets"]
  }

  depends_on = [null_resource.cluster_depends_on]
}

resource "null_resource" "cluster_depends_on" {
  count = var.create_eks ? 1 : 0
  triggers = {
    value = length(var.module_depends_on)
  }
}

resource "local_file" "kubeconfig" {
  count    = var.create && var.create_eks ? 1 : 0
  content  = local.kubeconfig
  filename = "~/.kube/config"
}

data "null_data_source" "policy" {
  count    = var.create && var.create_eks ? 1 : 0
  inputs = {
    tmpl = templatefile("${path.root}/templates/default_monitoring.yaml.tpl", {
      cluster_name = var.cluster_name
      region_name = var.region
    })
  }
}

data "null_data_source" "cluster_autoscaler_yaml" { # Generate the cluster autoscaler from a template
  count    = var.create && var.create_eks ? 1 : 0
  inputs = {
    tmpl = templatefile("${path.root}/templates/cluster-autoscaler.yaml.tpl", {
      cluster_name = var.cluster_name
    })
  }
}

data "tls_certificate" "cluster" {
  count    = var.create && var.create_eks ? 1 : 0
  url = element(concat(aws_eks_cluster.cluster.*.identity.0.oidc.0.issuer, list("")), count.index)
}

resource "aws_iam_openid_connect_provider" "cluster" { # We need an open id connect provider to allow our service account to assume an IAM role
  count    = var.create && var.create_eks ? 1 : 0
  client_id_list = ["sts.amazonaws.com"]
  thumbprint_list = element(concat([data.tls_certificate.cluster.*.certificates.0.sha1_fingerprint], []), count.index)
  url = element(concat(aws_eks_cluster.cluster.*.identity.0.oidc.0.issuer, list("")), count.index)
}

resource "null_resource" "generate_kubeconfig" { # Generate a kubeconfig (needs aws cli >=1.62 and kubectl)
  count    = var.create && var.create_eks ? 1 : 0
  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name ${var.cluster_name} --region ${var.region}"
  }
  depends_on = [aws_eks_cluster.cluster]
}

resource "null_resource" "aws_auth_configmap_install" { # Configure aws-auth-configmap
  count    = var.create && var.create_eks ? 1 : 0
  provisioner "local-exec" {
    command = "echo '${local.config_map_aws_auth}' > config_map_aws_auth.yaml && kubectl apply -f config_map_aws_auth.yaml && rm config_map_aws_auth.yaml"
  }
  depends_on = [aws_eks_cluster.cluster, null_resource.generate_kubeconfig]
}

resource "null_resource" "install_calico" { # The node won't enter the ready state without a CNI initialized
  count    = var.create && var.create_eks ? 1 : 0
  provisioner "local-exec" {
    command = "kubectl apply -f https://docs.projectcalico.org/v3.8/manifests/calico.yaml"
  }
  depends_on = [null_resource.generate_kubeconfig]
}

#kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml

resource "null_resource" "install_metrics_server" { # Install cluster monitoring templates
  count    = var.create && var.create_eks ? 1 : 0
  provisioner "local-exec" {
    command = "kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml"
  }
  depends_on = [null_resource.generate_kubeconfig]
}

resource "null_resource" "install_monitoring_manifest" { # Install cluster monitoring templates
  count    = var.create && var.create_eks ? 1 : 0
  provisioner "local-exec" {
    command = "echo ${element(data.null_data_source.policy.*.outputs["tmpl"], count.index)} > monitoring_template.yaml kubectl apply -f monitoring_template.yaml && rm monitoring_template.yaml"
  }
  depends_on = [null_resource.generate_kubeconfig]
}

resource "null_resource" "cluster_autoscaler_install" { # Install the cluster autoscaler
  count    = var.create && var.create_eks ? 1 : 0
  provisioner "local-exec" {
    command = "echo '${element(data.null_data_source.cluster_autoscaler_yaml.*.outputs["tmpl"], count.index)}' > cluster_autoscaler.yaml && kubectl apply -f cluster_autoscaler.yaml && rm cluster_autoscaler.yaml"
  }
  depends_on = [aws_eks_cluster.cluster, null_resource.generate_kubeconfig]
}

resource "null_resource" "tiller_install" { # Install the cluster autoscaler
  count    = var.create && var.create_eks ? 1 : 0
  provisioner "local-exec" {
    command = "kubectl apply -f ${path.root}/templates/tiller.yaml"
  }
  depends_on = [aws_eks_cluster.cluster, null_resource.generate_kubeconfig]
}

# resource "null_resource" "custom" {
#   count    = var.create && var.create_eks ? 1 : 0
#   depends_on    = [local_file.kubeconfig]

#   # change trigger to run every time
#   triggers = {
#     build_number = "${timestamp()}"
#   }

#   # download kubectl
#   provisioner "local-exec" {
#     command = <<EOF
#       set -e

#       curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/aws-iam-authenticator
#       chmod +x aws-iam-authenticator
#       mkdir -p $HOME/bin && cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$PATH:$HOME/bin

#       echo $PATH

#       aws-iam-authenticator

#       curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
#       chmod +x kubectl

#       ./kubectl apply -f 
#     EOF
#   }
# }