output "codebuild_id" {
  description = "Codebuild ID"
  value       = aws_codebuild_project.codebuild.*.id
}

output "codebuild_arn" {
  description = "Codebuild ARN"
  value       = aws_codebuild_project.codebuild.*.arn
}

output "codebuild_source_authorization_creds_id" {
  value = aws_codebuild_source_credential.authorization.*.id
}

output "codebuild_source_authorization_creds_arn" {
  value = aws_codebuild_source_credential.authorization.*.arn
}