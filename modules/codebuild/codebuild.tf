resource "aws_codebuild_project" "codebuild" {
  count          = var.create && var.resource_create ? 1 : 0
  name           = format("%s-%s-%s-%s", "codebuild", var.app, var.env, count.index + 1)
  description    = var.description # "Codebuild for the ECS Green/Blue Example app"
  service_role   = element(concat(var.service_role, list("")), count.index)
  encryption_key = var.encryption_key != "" ? element(concat(var.encryption_key, list("")), count.index) : null
  source_version = var.source_version != "" ? var.source_version : "master"

  # Artifacts
  dynamic "artifacts" {
    for_each = [var.artifacts]
    content {
      type                   = lookup(artifacts.value, "type", "CODEPIPELINE")
      artifact_identifier    = lookup(artifacts.value, "artifact_identifier", null)
      encryption_disabled    = lookup(artifacts.value, "encryption_disabled", false)
      override_artifact_name = lookup(artifacts.value, "override_artifact_name", true)
      location               = lookup(artifacts.value, "location", null)
      name                   = lookup(artifacts.value, "name", null)
      namespace_type         = lookup(artifacts.value, "namespace_type", "BUILD_ID")
      packaging              = lookup(artifacts.value, "packaging", "ZIP")
      path                   = lookup(artifacts.value, "path", null)
    }
  }

  cache {
    type     = lookup(var.cache, "type", "LOCAL")
    location = lookup(var.cache, "location", null) #TODO lookup(var.cache, "type") == "S3", set bucket prefix
    modes    = lookup(var.cache, "modes", lookup(var.cache, "type") == "LOCAL" ? ["LOCAL_SOURCE_CACHE"] : null)
  }

  dynamic "vpc_config" {
    for_each = length(var.vpc_config) > 0 ? [""] : []
    content {
      vpc_id             = lookup(var.vpc_config, "vpc_id", null)
      subnets            = lookup(var.vpc_config, "subnets", null)
      security_group_ids = lookup(var.vpc_config, "security_group_ids", null)
    }
  }

  dynamic "logs_config" {
    for_each = length(var.logs_config) > 0 ? [""] : []
    content {
      dynamic "cloudwatch_logs" {
        for_each = contains(keys(var.logs_config), "cloudwatch_logs") ? { key = var.logs_config["cloudwatch_logs"] } : {}
        content {
          status      = lookup(cloudwatch_logs.value, "status", "ENABLED")
          group_name  = lookup(cloudwatch_logs.value, "group_name", format("%s-%s-%s", "codebuild-loggroup", var.app, var.env))
          stream_name = lookup(cloudwatch_logs.value, "stream_name", format("%s-%s-%s", "codebuild-logstream", var.app, var.env))
        }
      }

      dynamic "s3_logs" {
        for_each = contains(keys(var.logs_config), "s3_logs") ? { key = var.logs_config["s3_logs"] } : {}
        content {
          status              = lookup(s3_logs.value, "status", "ENABLED")
          location            = format("%s/%s", lookup(s3_logs.value, "location", element(concat(var.default_log_location, list("")), 0)), "build-log")
          encryption_disabled = lookup(s3_logs.value, "encryption_disabled", false)
        }
      }
    }
  }

  dynamic "environment" {
    for_each = local.environment
    content {
      compute_type                = lookup(environment.value, "compute_type", var.build_compute_type)     #"BUILD_GENERAL1_SMALL"
      image                       = lookup(environment.value, "image", var.build_image)                   #"aws/codebuild/docker:18.09.0"
      type                        = lookup(environment.value, "type", var.build_environment_type)         #"LINUX_CONTAINER"
      privileged_mode             = lookup(environment.value, "privileged_mode", var.privileged_mode)     #true
      image_pull_credentials_type = lookup(environment.value, "image_pull_credentials_type", "CODEBUILD") #TODO var.private_registry ? "SERVICE_ROLE" : "CODEBUILD"
      certificate                 = lookup(environment.value, "certificate", null)

      # Registry Credential for private docker registery
      dynamic "registry_credential" {
        for_each = lookup(environment.value, "registry_credential", null) == null ? [] : [lookup(environment.value, "registry_credential")]
        content {
          credential          = registry_credential.value.credential          # Secret manager ARN
          credential_provider = registry_credential.value.credential_provider # SECRETS_MANAGER
        }
      }

      # Environment variables
      environment_variable {
        name  = "AWS_DEFAULT_REGION"
        value = local.region
      }

      environment_variable {
        name  = "TERRAFORM_VERSION"
        value = "0.12.28"
      }

      environment_variable {
        name  = "AWS_ACCOUNT_ID"
        value = local.account_id # data.aws_caller_identity.task_current.account_id
      }

      environment_variable {
        name  = "IMAGE_TAG"
        value = "latest"
      }


      dynamic "environment_variable" {
        for_each = length(lookup(environment.value, "variables")) == 0 ? [] : lookup(environment.value, "variables")
        content {
          name  = environment_variable.value.name
          value = environment_variable.value.value
          type  = lookup(environment_variable.value, "type", "PLAINTEXT")
        }
      }
    }
  }

  source {
    buildspec           = var.buildspec
    type                = var.source_type # CODEPIPELINE, GITHUB, BITBUCKET, CODECOMMIT, S3, NO_SOURCE
    location            = var.source_location
    report_build_status = var.source_type == "BITBUCKET" || var.source_type == "GITHUB" ? true : false
    git_clone_depth     = var.git_clone_depth != null ? var.git_clone_depth : null
    insecure_ssl        = var.insecure_ssl ? var.insecure_ssl : null

    dynamic "auth" {
      for_each = var.private_repository ? [""] : []
      content {
        type     = "OAUTH"
        resource = join("", aws_codebuild_source_credential.authorization.*.id)
      }
    }

    dynamic "git_submodules_config" {
      for_each = var.fetch_git_submodules ? [""] : []
      content {
        fetch_submodules = true
      }
    }
  }

  tags = merge(
    {
      "Name" = format("%s-%s-%s-%s", "codebuild", var.app, var.env, count.index + 1)
    },
    var.tags
  )
}

# locals {
#   registry_config = [{
#     credential          = var.credential != "" ? var.credential : null
#     credential_provider = var.credential_provider != "" ? var.credential_provider : null
#   }]
# }

resource "aws_codebuild_source_credential" "authorization" {
  count       = var.create && var.resource_create && var.private_repository ? 1 : 0
  auth_type   = var.source_credential_auth_type   # BASIC_AUTH, PERSONAL_ACCESS_TOKEN
  server_type = var.source_credential_server_type # BITBUCKET, GITHUB
  token       = var.source_credential_token       # app password for BITBUCKET, token for rest
  user_name   = var.source_credential_auth_type == "BASIC_AUTH" ? var.source_credential_user_name : null
}

data "aws_region" "build_region" {}
data "aws_caller_identity" "build_account" {}

locals {
  region          = var.region != "" ? var.region : data.aws_region.build_region.name
  account_id      = var.account_id != "" ? var.account_id : data.aws_caller_identity.build_account.account_id
  environment = [
    {
      compute_type                = lookup(var.environment, "compute_type", null)
      image                       = lookup(var.environment, "image", null)
      type                        = lookup(var.environment, "type", null)
      image_pull_credentials_type = lookup(var.environment, "image_pull_credentials_type", null)
      variables                   = lookup(var.environment, "environment_variables", null) == null ? var.default_env_vars : lookup(var.environment, "environment_variables")
      privileged_mode             = lookup(var.environment, "privileged_mode", null)
      certificate                 = lookup(var.environment, "certificate ", null)
      registry_credential         = lookup(var.environment, "registry_credential", null)
    }
  ]
}