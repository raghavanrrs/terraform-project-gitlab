resource "aws_vpc" "vpc" {
  count                = var.create && var.resource_create ? 1 : 0
  cidr_block           = var.cidr
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  tags = merge(
    {
      "Name"        = format("%s-%s-%s", var.name, var.env, count.index + 1)
      "Environment" = format("%s", var.env)
    },
    var.extra_tags,
    var.tags
  )
}

resource "aws_vpc_ipv4_cidr_block_association" "vpccidr" {
  count      = var.create && var.resource_create && length(var.secondary_cidr_blocks) > 0 ? length(var.secondary_cidr_blocks) : 0
  vpc_id     = aws_vpc.vpc[0].id
  cidr_block = element(var.secondary_cidr_blocks, count.index)
}