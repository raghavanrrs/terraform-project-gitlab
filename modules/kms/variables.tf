variable "resource_create" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  default     = false
}

variable "create" {
  default     = false
  description = "Master control variable if VPC should be created (it affects almost all resources)"
}

variable "app" {
  default     = "sample"
  description = "Name for application"
  type        = string
}

variable "env" {
  type        = string
  description = "Enviroment name"
  default     = "dev"
}

variable "policy" {
  type        = string
  default     = ""
  description = "A valid policy JSON document. For more information about building AWS IAM policy documents with Terraform."
}

variable "deletion_window_in_days" {
  default     = 7
  description = "Number of days the key will be scheduled for deletion"
  type        = number
}

variable "description" {
  default     = "description for kms key"
  description = "description for kms key"
  type        = string
}

variable "alias" {
  type        = string
  description = "(optional) alias for your kms starts with alias/"
  default     = ""
}

variable "tags" {
  type = map
  default = {
    "Terraform" : "true"
  }
}