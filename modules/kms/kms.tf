resource "random_string" "randomstr" {
  count            = var.create && var.resource_create ? 1 : 0
  length           = 8
  special          = false
  override_special = "/_+=.@-"
}

resource "aws_kms_key" "kmskey" {
  count                   = var.create && var.resource_create ? 1 : 0
  description             = var.description
  deletion_window_in_days = var.deletion_window_in_days
  policy                  = var.policy != "" ? var.policy : null
  tags = merge(
    {
      "Name" = format("%s-%s-%s%s", var.app, var.env != "" ? var.env : terraform.workspace, "kmskey", element(concat(random_string.randomstr.*.result, list("")), count.index))
    },
    var.tags
  )
}

resource "aws_kms_alias" "keyalias" {
  count         = var.create && var.resource_create ? 1 : 0
  name          = format("%s/%s-%s%s", "alias", var.app, var.env != "" ? var.env : terraform.workspace, element(concat(random_string.randomstr.*.result, list("")), count.index))
  target_key_id = join("", aws_kms_key.kmskey.*.key_id)
}