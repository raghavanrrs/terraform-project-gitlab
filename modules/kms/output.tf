output "keyarn" {
  description = "KMS key ARN"
  value       = aws_kms_key.kmskey.*.arn
}

output "keyid" {
  description = "KMS key ID"
  value       = aws_kms_key.kmskey.*.key_id
}

output "keyaliasarn" {
  description = "KMS key alias ARN"
  value       = aws_kms_alias.keyalias.*.arn
}

output "kms_target_key_arn" {
  value = aws_kms_alias.keyalias.*.target_key_arn
}