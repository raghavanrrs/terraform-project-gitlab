# variable "kms_key_id" {
#     default = ""
# }

variable "resource_create" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  default     = false
}

variable "create" {
  default     = false
  description = "Master control variable if VPC should be created (it affects almost all resources)"
}

variable "tags" {
  type    = map
  default = {}
}

variable "name" {
  description = "EIP name."
  type        = string
  default     = ""
}

variable "pass_version" {
  default     = 1
  description = "Password version. Increment this to trigger a new password."
  type        = number
}

variable "app" {
  default     = "go"
  description = "Name for application"
  type        = string
}

variable "alias" {
  type        = string
  description = "(optional) alias for your kms starts with alias/"
  default     = ""
}

variable "env" {
  type        = string
  description = "Enviroment name"
  default     = "dev"
}