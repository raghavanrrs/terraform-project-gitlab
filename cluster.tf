provider "kubernetes" {
  load_config_file       = "false"
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

data "aws_eks_cluster" "cluster" {
  depends_on = [module.eks_cluster]
  name = local.cluster_name
}

data "aws_eks_cluster_auth" "cluster" {
  depends_on = [module.eks_cluster]
  name = local.cluster_name
}

data "aws_iam_policy_document" "k8s_default" {
  version = "2012-10-17"
  statement {
    sid    = "EnableIAMUserPermissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions = [
      "kms:DescribeKey",
      "kms:GenerateDataKey*",
      "kms:Encrypt",
      "kms:ReEncrypt*",
      "kms:Decrypt"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "AllowCloudTrailToEncryptLogs"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["kms:GenerateDataKey*"]
    resources = ["*"]
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
      values   = ["arn:aws:cloudtrail:*:${data.aws_caller_identity.task_current.account_id}:trail/*"]
    }
  }

  statement {
    sid    = "AllowCloudTrailToDescribeKey"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["kms:DescribeKey"]
    resources = ["*"]
  }

  statement {
    sid    = "AllowPrincipalsAccountToDecryptLogFiles"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions = [
      "kms:Decrypt",
      "kms:ReEncryptFrom"
    ]
    resources = ["*"]
    condition {
      test     = "StringEquals"
      variable = "kms:CallerAccount"
      values = [
        "${data.aws_caller_identity.task_current.account_id}"
      ]
    }
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
      values   = ["arn:aws:cloudtrail:*:${data.aws_caller_identity.task_current.account_id}:trail/*"]
    }
  }

  statement {
    sid    = "AllowAliasCreation"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions   = ["kms:CreateAlias"]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "assume_by_eks" {
  statement {
    sid     = "AllowAssumeByEKS"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

# module "eks_cmk_kms_log_group" {
#   source          = "./modules/kms"
#   create          = var.create
#   resource_create = var.task
#   app             = "eks-log-group"
#   env             = terraform.workspace
#   description     = "CMK for EKS log group"
#   alias           = "alias/eksloggroupkmskey"
#   #policy          = data.aws_iam_policy_document.k8s_default.json
# }

# module "eks_cloudwatch_log" {
#   source            = "./modules/cloudwatch"
#   create            = var.create
#   resource_create   = var.task
#   app               = "eks-log-group"
#   env               = terraform.workspace
#   resource_type     = "eks"
#   resourcename      = local.cluster_name
#   stream_names      = ["eks-cluser-stream-1"]
#   retention_in_days = 1
#   #encryption_key    = module.eks_cmk_kms_log_group.keyarn
# }

module "eks_cluster_role" {
  source             = "./modules/iam/service_role"
  create             = var.create
  resource_create    = var.task
  name               = "eks-cluster-role"
  path = "/"
  tags               = var.tags
  assume_role_policy = data.aws_iam_policy_document.assume_by_eks.json
}

module "eks_cluster_role_policy_attachment" {
  source          = "./modules/iam/policy_attachment"
  create          = var.create
  resource_create = var.task
  roles           = module.eks_cluster_role.role_name
  policy_arn      = ["arn:${data.aws_partition.task_current.partition}:iam::aws:policy/AmazonEKSClusterPolicy", "arn:${data.aws_partition.task_current.partition}:iam::aws:policy/AmazonEKSServicePolicy", "arn:${data.aws_partition.task_current.partition}:iam::aws:policy/AmazonEKSVPCResourceController"]
}

module "eks_cluster_kms" {
  source          = "./modules/kms"
  create          = var.create
  resource_create = var.task
  app             = "eks-kms-key"
  env             = terraform.workspace
  description     = "CMK for EKS Cluster"
  #alias           = "alias/eksclusterkmskey"
  #policy          = data.aws_iam_policy_document.k8s_default.json
}

data "aws_iam_policy_document" "eks_cluster" {
  version = "2012-10-17"
  statement {
    sid    = "EnableIAMUserPermissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.task_current.account_id}:root"
      ]
    }
    actions = ["kms:*"]
    resources = ["*"]
  }

  statement {
    sid    = "AllowAccessForKeyAdministrators"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.task_current.account_id}:root",
        element(concat(module.eks_cluster_role.role_arn, list("")), 0),
      ]
    }
    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ]
    resources = ["*"]
  }

  statement {
    sid    = "AllowUseOfKey"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.task_current.account_id}:root",
        element(concat(module.eks_cluster_role.role_arn, list("")), 0),]
    }
    actions = [
      "kms:DescribeKey",
      "kms:GenerateDataKey*",
      "kms:Encrypt",
      "kms:ReEncrypt*",
      "kms:Decrypt"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "AllowPrincipalsAccountDecryptLogFiles"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.task_current.account_id}:root",
        element(concat(module.eks_cluster_role.role_arn, list("")), 0),
      ]
    }
    actions = [
      "kms:Decrypt",
      "kms:ReEncryptFrom"
    ]
    resources = ["*"]
    condition {
      test     = "StringEquals"
      variable = "kms:CallerAccount"
      values = [
        "${data.aws_caller_identity.task_current.account_id}"
      ]
    }
  }
  statement {
    sid    = "AllowAttachmentOfPersistentResources"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.task_current.account_id}:root",
        element(concat(module.eks_cluster_role.role_arn, list("")), 0),
      ]
    }
    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant"
    ]
    resources = ["*"]
    condition {
      test     = "Bool"
      variable = "kms:GrantIsForAWSResource"
      values = ["true"]
    }
  }
}

# module "ekscluster_role_policy_kms" {
#   source          = "./modules/iam/policy"
#   create          = var.create
#   resource_create = var.task
#   name            = "ekscluster_role_policy_kms"
#   path            = "/"
#   description     = "EKS cluster role policy for KMS access"

#   policy = data.aws_iam_policy_document.eks_cluster.json
# }

# module "eks_cluster_role_kms_policy_attachment" {
#   source          = "./modules/iam/policy_attachment"
#   create          = var.create
#   resource_create = var.task
#   roles           = module.eks_cluster_role.role_arn
#   policy_arn      = module.ekscluster_role_policy_kms.arn
# }

data "template_file" "eks_launch_template_userdata" {
  count          = var.create && var.task ? 1 : 0
  template = file("${path.module}/templates/bootstrap.sh.tpl")

  vars = {
    cluster_name        = module.eks_cluster.eks_cluster_name
    endpoint            = module.eks_cluster.eks_cluster_endpoint
    cluster_auth_base64 = module.eks_cluster.eks_cluster_certificate_authority_data

    bootstrap_extra_args = ""
    kubelet_extra_args   = ""
  }
}

module "eks_cluster_security_group" {
  source                 = "./modules/network/securitygroup"
  create                 = var.create
  resource_create        = var.task
  vpc_id                 = module.task_vpc_devops.vpc_id
  tags                   = var.tags
  env                    = var.env
  name                   = "eks-cluster-sg"
  description            = "EKS Cluster SG"
  revoke_rules_on_delete = true
}

module "eks_cluster_sg_ingress" {
  source          = "./modules/network/sgrules"
  create          = var.create
  resource_create = var.task
  sg_id           = module.eks_cluster_security_group.sg_id
  type            = "ingress"
  ingress_rules   = ["https-443-tcp"] # var.postgres_ingress_rules
  cidr_blocks     = [var.cidr]
  #source_security_group_id = module.task_sg_devops.sg_id
}

module "eks_cluster_sg_egress" {
  source          = "./modules/network/sgrules"
  create          = var.create
  resource_create = var.task
  sg_id           = module.eks_cluster_security_group.sg_id
  ingress_rules   = ["all-tcp", "all-udp"]
  cidr_blocks     = ["0.0.0.0/0"]
}

module "eks_cluster" {
  source = "./modules/eks"
  #cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"] # enforced the logging default
  service_role    = module.eks_cluster_role.role_arn
  create          = var.create
  create_eks      = var.task
  cluster_name    = local.cluster_name
  cluster_version = "1.17"
  region = var.region
  tags = {
    Environment = terraform.workspace
    owner       = "ccoe"
    stage       = "plan"
  }
  cluster_security_group_id = module.eks_cluster_security_group.sg_id
  subnets                   = [module.task_sub_private_devops.private_subnet_id, module.task_sub_public_devops.public_subnet_id]
  key_arn                   = module.eks_cluster_kms.keyarn
  module_depends_on         = [module.eks_cluster_role_policy_attachment]
  node_role = module.nodegroup_instance_role.role_arn
}

output "kubecert" {
  value = module.eks_cluster.eks_cluster_certificate_authority_data
}

output "config_map_aws_auth" {
  value = module.eks_cluster.config_map_aws_auth
}

output "kubeconfig" {
  value = module.eks_cluster.kubeconfig
}

/**
 * NEEDS KUBECTL AND AWS CLI INSTALLED.
 # Generate a kubeconfig (needs aws cli >=1.62 and kubectl)
 */

# resource "null_resource" "generate_kubeconfig" {
#   count          = var.create && var.task ? 1 : 0
#   provisioner "local-exec" {
#     command = "aws eks update-kubeconfig --name ${module.eks_cluster.eks_cluster_name} --region ${var.region}"
#   }
#   depends_on = [module.eks_cluster]
# }