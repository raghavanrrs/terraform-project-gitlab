terraform {
  required_version = ">= 0.12.28"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.13"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "~> 1.3.0"
    }
    template = {
      source  = "hashicorp/template"
      version = "~> 2.1.2"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 2.3.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 2.2.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 2.1.2"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 1.13.3"
    }
    http = {
      source  = "hashicorp/http"
      version = "~> 2.0.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.0.0"
    }
  }
}

provider "aws" {
  version                 = "~> 3.13"
  region                  = var.region
  shared_credentials_file = "c:\\Users\raghavan_r\\.aws\\credentials"
  profile                 = "tfassesment"
}

# terraform {
#   backend "s3" {}
# }

locals {
  task_max_subnet_length = max(
    length(var.private_subnets),
    length(var.database_subnets)
  )
  task_nat_gateway_count = var.single_nat_gateway ? 1 : var.one_nat_gateway_per_az ? length(data.aws_availability_zones.task_availableaz.names) : local.task_max_subnet_length
  cluster_name           = "${var.env}-cluster"
  current_account = data.aws_caller_identity.task_current.account_id
}

data "aws_kms_alias" "task_ssm" {
  name = "alias/aws/ssm"
}

data "aws_kms_alias" "task_ebs" {
  name = "alias/aws/ebs"
}

data "aws_kms_alias" "s3" {
  name = "alias/aws/s3"
}

data "aws_elb_service_account" "main" {}

data "aws_region" "task_current" {}

data "aws_availability_zones" "task_availableaz" {
  state = "available"
}

data "aws_ami" "task_amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

data "aws_partition" "task_current" {}

data "aws_caller_identity" "task_current" {}

data "http" "workstation-external-ip" {
  url = "http://ipv4.icanhazip.com"
}

# Override with variable or hardcoded value if necessary
locals {
  workstation-external-cidr = "${chomp(data.http.workstation-external-ip.body)}/32"
}
# data "archive_file" "init" {
#   type        = "zip"
#   source_dir  = "${path.root}/TechChallengeApp"
#   output_path = "${path.root}/TechChallengeApp.zip"
# }

# module "rds_db_secret_manager" {
#   source          = "./modules/secret-manager"
#   create          = var.create
#   resource_create = var.task
#   #   kms_key_id = data.aws_kms_alias.task_ssm.arn
#   name = "db-secret"
#   tags = var.tags
# }

module "task_databucket" {
  source                  = "./modules/s3"
  bucket_prefix           = "databucket"
  s3_bucket_force_destroy = true
  create                  = var.create
  resource_create         = var.task
  kms_arn                 = data.aws_kms_alias.s3.arn
  tags                    = var.tags
  sse_algorithm           = "AES256"
  policy                  = <<EOF
{
  "Version": "2012-10-17",
  "Id": "MYDATABUCKETPOLICY",
  "Statement": [
    {
      "Sid": "ELBAllow",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${data.aws_elb_service_account.main.arn}"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${var.create && var.task && length(module.task_databucket.bucket_id) > 0 ? element(concat(module.task_databucket.bucket_id, list("")), 0) : "*"}/logs/AWSLogs/${data.aws_caller_identity.task_current.account_id}/*"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "delivery.logs.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${var.create && var.task && length(module.task_databucket.bucket_id) > 0 ? element(concat(module.task_databucket.bucket_id, list("")), 0) : "*"}/logs/AWSLogs/${data.aws_caller_identity.task_current.account_id}/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "delivery.logs.amazonaws.com"
      },
      "Action": "s3:GetBucketAcl",
      "Resource": "arn:aws:s3:::${var.create && var.task && length(module.task_databucket.bucket_id) > 0 ? element(concat(module.task_databucket.bucket_id, list("")), 0) : "*"}"
    },
    {
      "Sid": "DenyUnEncryptedObjectUploads",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${var.create && var.task && length(module.task_databucket.bucket_id) > 0 ? element(concat(module.task_databucket.bucket_id, list("")), 0) : "*"}/*",
      "Condition": {
          "StringNotEquals": {
              "s3:x-amz-server-side-encryption": "aws:kms"
          }
      }
    },
    {
      "Sid": "DenyInsecureConnections",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::${var.create && var.task && length(module.task_databucket.bucket_id) > 0 ? element(concat(module.task_databucket.bucket_id, list("")), 0) : "*"}/*",
      "Condition": {
          "Bool": {
              "aws:SecureTransport": "false"
          }
      }
    }
  ]
}
EOF
}

# module "task_application_upload_archive" {
#   source            = "./modules/file_upload_s3"
#   create            = var.create
#   resource_create   = var.task
#   prefix            = "artefacts"
#   bucket_name       = join(" ", module.task_databucket.bucket_id)
#   tags              = var.tags
#   file_name         = "${path.root}/TechChallengeApp.zip"
#   encryption_method = "AES256"
#   force_destroy     = true
# }

# module "task_instance_role_iam_policy" {
#   source          = "./modules/iam/policy"
#   create          = var.create
#   resource_create = var.task
#   name            = "task-custom-policy-ec2-s3"
#   path            = "/"
#   description     = "My custom policy"

#   policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:GetObject",
#                 "kms:Decrypt",
#                 "kms:ListKeyPolicies",
#                 "kms:ListRetirableGrants",
#                 "kms:Encrypt",
#                 "kms:GenerateDataKey",
#                 "kms:GenerateDataKeyWithoutPlaintext",
#                 "kms:DescribeKey",
#                 "kms:Verify",
#                 "kms:GenerateDataKeyPairWithoutPlaintext",
#                 "kms:GenerateDataKeyPair",
#                 "kms:ListGrants"
#             ],
#             "Resource": [
#                 "arn:aws:s3:::aws-ssm-region/*",
#                 "arn:aws:s3:::aws-windows-downloads-region/*",
#                 "arn:aws:s3:::amazon-ssm-region/*",
#                 "arn:aws:s3:::amazon-ssm-packages-region/*",
#                 "arn:aws:s3:::region-birdwatcher-prod/*",
#                 "arn:aws:s3:::aws-ssm-distributor-file-region/*",
#                 "arn:aws:s3:::patch-baseline-snapshot-region/*",
#                 "arn:aws:kms:${data.aws_region.task_current.name}:${data.aws_caller_identity.task_current.account_id}:key/*"
#             ]
#         },
#         {
#             "Sid": "VisualEditor0",
#             "Effect": "Allow",
#             "Action": [
#                 "kms:ListKeys",
#                 "iam:PassRole",
#                 "s3:ListAllMyBuckets",
#                 "kms:ListAliases"
#             ],
#             "Resource": "*"
#         },
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:PutObject",
#                 "s3:GetObject",
#                 "s3:GetEncryptionConfiguration",
#                 "s3:ListBucket",
#                 "s3:GetBucketLocation",
#                 "s3:PutObjectAcl"
#             ],
#             "Resource": [
#                 "arn:aws:s3:::${var.create && var.task && length(module.task_databucket.bucket_id) > 0 ? element(concat(module.task_databucket.bucket_id, list("")), 0) : "*"}",
#                 "arn:aws:s3:::${var.create && var.task && length(module.task_databucket.bucket_id) > 0 ? format("%s/%s", element(concat(module.task_databucket.bucket_id, list("")), 0), "*") : "*"}"
#             ]
#         }
#     ]
# }
# EOF
# }

# module "task_role_policy_attachment" {
#   source          = "./modules/iam/policy_attachment"
#   create          = var.create
#   resource_create = var.task
#   roles           = module.task_sg_devops_instance_role.role_name
#   policy_arn      = module.task_instance_role_iam_policy.arn
# }

# module "task_managed_role_policy_attachment" {
#   source          = "./modules/iam/policy_attachment"
#   create          = var.create
#   resource_create = var.task
#   roles           = module.task_sg_devops_instance_role.role_name
#   policy_arn      = ["arn:${data.aws_partition.task_current.partition}:iam::aws:policy/AmazonSSMManagedInstanceCore"]
# }

# module "task_managed_secret_role_policy_attachment" {
#   source          = "./modules/iam/policy_attachment"
#   create          = var.create
#   resource_create = var.task
#   roles           = module.task_sg_devops_instance_role.role_name
#   policy_arn      = ["arn:${data.aws_partition.task_current.partition}:iam::aws:policy/SecretsManagerReadWrite"]
# }

module "task_vpc_devops" {
  source          = "./modules/network/vpc"
  create          = var.create
  resource_create = var.task
  cidr            = var.cidr
  name            = var.name
  env             = var.env
  tags            = var.tags
  extra_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  }
}

module "task_sub_public_devops" {
  source                  = "./modules/network/subnets"
  create                  = var.create
  resource_create         = var.task
  public_subnets          = var.public_subnets
  azs                     = data.aws_availability_zones.task_availableaz.names
  vpc_id                  = module.task_vpc_devops.vpc_id
  one_nat_gateway_per_az  = var.one_nat_gateway_per_az
  single_nat_gateway      = var.single_nat_gateway
  map_public_ip_on_launch = false
  subnet_suffix           = "task-subnet-public-devops"
  tags                    = var.tags
  extra_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "true"
  }
}

module "task_igw" {
  source          = "./modules/network/igw"
  public_subnets  = var.public_subnets
  create          = var.create
  resource_create = var.task
  vpc_id          = module.task_vpc_devops.vpc_id
  tags            = var.tags
  name            = format("%s-%s", var.env, "igw")
}

module "task_public_rt" {
  source          = "./modules/network/rt"
  public_subnets  = var.public_subnets
  create          = var.create
  resource_create = var.task
  vpc_id          = module.task_vpc_devops.vpc_id
  tags            = var.tags
  name            = format("%s-%s", var.env, "task-publicrt")
}

module "task_public_rt_association" {
  source          = "./modules/network/rtassoc"
  create          = var.create
  resource_create = var.task
  subnet_id       = module.task_sub_public_devops.public_subnet_id
  rt_id           = module.task_public_rt.public_rt_id
}

module "task_public_subnet_route" {
  source          = "./modules/network/route"
  create          = var.create
  resource_create = var.task
  create_igw      = true
  igw_id          = module.task_igw.igw_id
  subnet_id       = module.task_sub_public_devops.public_subnet_id
  rt_id           = module.task_public_rt.public_rt_id
}

module "task_nat_gw_eip" {
  source          = "./modules/network/eip"
  create          = var.create
  resource_create = var.task
  eip_count       = var.single_nat_gateway ? 1 : var.one_nat_gateway_per_az ? length(data.aws_availability_zones.task_availableaz.names) : local.task_max_subnet_length
  name            = "task-ngw-eip"
  tags            = var.tags
}

module "task_private_subnet_nat_gateway" {
  source          = "./modules/network/natgw"
  create          = var.create
  resource_create = var.task
  create_ngw      = var.one_nat_gateway_per_az || var.single_nat_gateway ? true : false
  gateway_count   = var.single_nat_gateway ? 1 : var.one_nat_gateway_per_az ? length(data.aws_availability_zones.task_availableaz.names) : local.task_max_subnet_length
  subnet_id       = module.task_sub_public_devops.public_subnet_id
  allocation_id   = module.task_nat_gw_eip.eip_id
  name            = "devops-ngw"
  tags            = var.tags
}

module "task_sub_private_devops" {
  source          = "./modules/network/subnets"
  create          = var.create
  resource_create = var.task
  private_subnets = var.private_subnets
  azs             = data.aws_availability_zones.task_availableaz.names
  vpc_id          = module.task_vpc_devops.vpc_id
  subnet_suffix   = "task-subnet-private-devops"
  tags            = var.tags
  extra_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "true"
  }
}

module "task_private_rt" {
  source          = "./modules/network/rt"
  private_subnets = var.private_subnets
  gateway_count   = local.task_nat_gateway_count
  create          = var.create
  resource_create = var.task
  azs             = data.aws_availability_zones.task_availableaz.names
  vpc_id          = module.task_vpc_devops.vpc_id
  tags            = var.tags
  name            = format("%s-%s", var.env, "task-privatert")
}

module "task_private_rt_association" {
  source          = "./modules/network/rtassoc"
  create          = var.create
  resource_create = var.task
  subnet_id       = module.task_sub_private_devops.private_subnet_id
  rt_id           = module.task_private_rt.private_rt_id
}

module "task_private_subnet_route" {
  source          = "./modules/network/route"
  create          = var.create
  resource_create = var.task
  create_ngw      = var.one_nat_gateway_per_az || var.single_nat_gateway ? true : false
  nat_gw_id       = module.task_private_subnet_nat_gateway.nat_id
  subnet_id       = module.task_sub_private_devops.private_subnet_id
  rt_id           = module.task_private_rt.private_rt_id
}

module "task_sub_db_devops" {
  source          = "./modules/network/subnets"
  create          = var.create
  resource_create = var.task
  private_subnets = var.database_subnets
  azs             = data.aws_availability_zones.task_availableaz.names
  vpc_id          = module.task_vpc_devops.vpc_id
  subnet_suffix   = "task-sub-db-devops"
  tags            = var.tags
  env             = var.env
}

module "task_db_private_rt" {
  source          = "./modules/network/rt"
  private_subnets = var.private_subnets
  gateway_count   = local.task_nat_gateway_count
  create          = var.create
  resource_create = var.task
  azs             = data.aws_availability_zones.task_availableaz.names
  vpc_id          = module.task_vpc_devops.vpc_id
  tags            = var.tags
  name            = format("%s-%s", var.env, "task-privatert-db")
}

module "task_private_db_rt_association" {
  source          = "./modules/network/rtassoc"
  create          = var.create
  resource_create = var.task
  subnet_id       = module.task_sub_db_devops.private_subnet_id
  rt_id           = module.task_db_private_rt.private_rt_id
}

module "task_private_db_subnet_route" {
  source          = "./modules/network/route"
  create          = var.create
  resource_create = var.task
  create_ngw      = var.one_nat_gateway_per_az || var.single_nat_gateway ? true : false
  nat_gw_id       = module.task_private_subnet_nat_gateway.nat_id
  subnet_id       = module.task_sub_db_devops.private_subnet_id
  rt_id           = module.task_db_private_rt.private_rt_id
}

module "task_sg_devops" {
  source                 = "./modules/network/securitygroup"
  create                 = var.create
  resource_create        = var.task
  vpc_id                 = module.task_vpc_devops.vpc_id
  tags                   = var.tags
  env                    = var.env
  name                   = "task-sg-devops"
  description            = "SG devops group-task"
  revoke_rules_on_delete = true
}

module "task_sg_devops_ingress" {
  source          = "./modules/network/sgrules"
  create          = var.create
  resource_create = var.task
  sg_id           = module.task_sg_devops.sg_id
  type            = "ingress"
  ingress_rules   = var.public_ingress_rules
  cidr_blocks     = [var.cidr]
}

module "task_sg_devops_egress" {
  source          = "./modules/network/sgrules"
  create          = var.create
  resource_create = var.task
  sg_id           = module.task_sg_devops.sg_id
  ingress_rules   = ["all-tcp", "all-udp"]
  cidr_blocks     = ["0.0.0.0/0"]
}

# module "task_sg_devops_keygen" {
#   source          = "./modules/keygen"
#   create          = var.create
#   resource_create = var.task
# }

# module "task_sg_devops_keypair" {
#   source          = "./modules/keypair"
#   create          = var.create
#   resource_create = var.task
#   key_name        = format("%s-%s", var.env, "task-keypair")
#   public_key      = module.task_sg_devops_keygen.key_public_openssh
# }

# module "task_sg_devops_ssm" {
#   source          = "./modules/ssm"
#   create          = var.create
#   resource_create = var.task
#   name            = format("/%s-%s/%s/%s", "task-vpc", var.env, "keypair", "private")
#   type            = "SecureString"
#   value           = module.task_sg_devops_keygen.key_private_pem
#   key_id          = data.aws_kms_alias.task_ssm.arn
#   description     = "task private key"
#   overwrite       = true
#   tags            = var.tags
# }

# module "task_sg_devops_instance_role" {
#   source          = "./modules/iam/instance_role"
#   create          = var.create
#   resource_create = var.task
#   name            = "task-ec2-role"
#   tags            = var.tags
# }

# module "task_sg_devops_instance_profile" {
#   source          = "./modules/iam/instance_profile"
#   create          = var.create
#   resource_create = var.task
#   name            = "task-ec2-devops-role"
#   role            = module.task_sg_devops_instance_role.role_name
# }

# module "task_elb_sg" {
#   source                 = "./modules/network/securitygroup"
#   create                 = var.create
#   resource_create        = var.task
#   vpc_id                 = module.task_vpc_devops.vpc_id
#   tags                   = var.tags
#   env                    = var.env
#   name                   = "task-elb-sg"
#   description            = "SG devops group-task elb"
#   revoke_rules_on_delete = true
# }

# module "task_elb_sg_ingress" {
#   source          = "./modules/network/sgrules"
#   create          = var.create
#   resource_create = var.task
#   sg_id           = module.task_elb_sg.sg_id
#   type            = "ingress"
#   ingress_rules   = ["http-80-tcp"]
#   cidr_blocks     = ["0.0.0.0/0"]
# }

# module "task_elb_sg_egress" {
#   source          = "./modules/network/sgrules"
#   create          = var.create
#   resource_create = var.task
#   sg_id           = module.task_elb_sg.sg_id
#   ingress_rules   = ["all-tcp", "all-udp"]
#   cidr_blocks     = ["0.0.0.0/0"]
# }

# # ################# DB #######################
# module "task_db_sg" {
#   source                 = "./modules/network/securitygroup"
#   create                 = var.create
#   resource_create        = var.task
#   vpc_id                 = module.task_vpc_devops.vpc_id
#   tags                   = var.tags
#   env                    = var.env
#   name                   = "task-dbsg-devops"
#   description            = "DB SG devops group-task"
#   revoke_rules_on_delete = true
# }

# module "task_db_sg_ingress" {
#   source                   = "./modules/network/sgrules"
#   create                   = var.create
#   resource_create          = var.task
#   sg_id                    = module.task_db_sg.sg_id
#   type                     = "ingress"
#   ingress_rules            = var.postgres_ingress_rules
#   source_security_group_id = module.task_sg_devops.sg_id
# }

# module "task_db_sg_egress" {
#   source          = "./modules/network/sgrules"
#   create          = var.create
#   resource_create = var.task
#   sg_id           = module.task_db_sg.sg_id
#   ingress_rules   = ["all-tcp", "all-udp"]
#   cidr_blocks     = ["0.0.0.0/0"]
# }

# module "tas2_db_subnetgrp" {
#   source          = "./modules/network/db_subnet_grp"
#   create          = var.create
#   resource_create = var.task
#   name            = "task-db-subnetgrp"
#   subnet_ids      = module.task_sub_db_devops.private_subnet_id
#   tags            = var.tags
# }

# module "task_rds_posgresql" {
#   # Configure AWS RDS
#   source            = "./modules/RDS/Postgresql"
#   create            = var.create
#   resource_create   = var.task
#   alloc_storage     = "20"
#   storage_type      = "gp2"
#   storage_encrypted = true
#   engine            = "postgres"
#   engine_version    = "12.4"
#   instance_class    = var.db_instance_class
#   subnet_group      = module.tas2_db_subnetgrp.db_subnetgrp_name
#   public            = "false"
#   security_group    = module.task_db_sg.sg_id
#   kms_arn           = data.aws_kms_alias.task_ebs.arn
#   multi_az          = true
#   AZ                = data.aws_availability_zones.task_availableaz.names[0]
#   identifier      = "app"
#   dbname          = "app"
#   dbuser          = var.database_username
#   dbpassword      = module.rds_db_secret_manager.db_pass
#   databasename    = "app"
#   parameter_group = "postgres12"
#   create_monitoring_role = true
#   skip_final_snapshot        = "true"
#   tags            = var.tags
# }

# data "template_file" "bootstrap" {
#   count    = var.create && var.task ? 1 : 0
#   template = file("${path.root}/scripts/bake_script.sh.tpl")
#   vars = {
#     rds_hostname = element(concat(module.task_rds_posgresql.rds_endpoint, list("")), 0)
#     secret_id = element(concat(module.rds_db_secret_manager.secret_name, list("")), 0)
#     db_name = element(concat(module.task_rds_posgresql.rds_db_name, list("")), 0)
#     rds_username = element(concat(module.task_rds_posgresql.rds_username, list("")), 0)
#     app_port = var.application_port
#     repo_url = var.repo_url
#     commit_id = var.app_commit_id
#   }
# }

# data "aws_ami" "amazon_linux_ecs" {
#   most_recent = true

#   owners = ["amazon"]

#   filter {
#     name   = "name"
#     values = ["amzn-ami-*-amazon-ecs-optimized"]
#   }

#   filter {
#     name   = "owner-alias"
#     values = ["amazon"]
#   }
# }


# resource "aws_ecs_cluster" "this" {
#   name = "example-cluster"
# }

# data "aws_iam_policy_document" "assume_by_ecs" {
#   statement {
#     sid     = "AllowAssumeByEcsTasks"
#     effect  = "Allow"
#     actions = ["sts:AssumeRole"]

#     principals {
#       type        = "Service"
#       identifiers = ["ecs-tasks.amazonaws.com"]
#     }
#   }
# }

# data "aws_iam_policy_document" "execution_role" {
#   statement {
#     sid    = "AllowECRPull"
#     effect = "Allow"

#     actions = [
#       "ecr:GetDownloadUrlForLayer",
#       "ecr:BatchGetImage",
#       "ecr:BatchCheckLayerAvailability",
#     ]

#     resources = [data.aws_ecr_repository.this.arn]
#   }

#   statement {
#     sid    = "AllowECRAuth"
#     effect = "Allow"

#     actions = ["ecr:GetAuthorizationToken"]

#     resources = ["*"]
#   }

#   statement {
#     sid    = "AllowLogging"
#     effect = "Allow"

#     actions = [
#       "logs:CreateLogStream",
#       "logs:PutLogEvents",
#     ]

#     resources = ["*"]
#   }
# }

# data "aws_iam_policy_document" "task_role" {
#   statement {
#     sid    = "AllowDescribeCluster"
#     effect = "Allow"

#     actions = ["ecs:DescribeClusters"]

#     resources = [aws_ecs_cluster.this.arn]
#   }
# }

# resource "aws_iam_role" "execution_role" {
#   name               = "ecs-example-execution-role"
#   assume_role_policy = data.aws_iam_policy_document.assume_by_ecs.json
# }

# resource "aws_iam_role_policy" "execution_role" {
#   role   = aws_iam_role.execution_role.name
#   policy = data.aws_iam_policy_document.execution_role.json
# }

# resource "aws_iam_role" "task_role" {
#   name               = "ecs-example-task-role"
#   assume_role_policy = data.aws_iam_policy_document.assume_by_ecs.json
# }

# resource "aws_iam_role_policy" "task_role" {
#   role   = aws_iam_role.task_role.name
#   policy = data.aws_iam_policy_document.task_role.json
# }

# resource "aws_ecs_task_definition" "this" {
#   family                   = "green-blue-ecs-example"
#   container_definitions    = module.container_definition.json
#   execution_role_arn       = aws_iam_role.execution_role.arn
#   task_role_arn            = aws_iam_role.task_role.arn
#   network_mode             = "awsvpc"
#   cpu                      = "256"
#   memory                   = "512"
#   requires_compatibilities = ["FARGATE"]
# }



# module "alb" {
#   source = "../../"

#   name = "complete-alb-${random_pet.this.id}"

#   load_balancer_type = "application"

#   vpc_id          = data.aws_vpc.default.id
#   security_groups = [module.security_group.this_security_group_id]
#   subnets         = data.aws_subnet_ids.all.ids

#   //  # See notes in README (ref: https://github.com/terraform-providers/terraform-provider-aws/issues/7987)
#   //  access_logs = {
#   //    bucket = module.log_bucket.this_s3_bucket_id
#   //  }

#   http_tcp_listeners = [
#     # Forward action is default, either when defined or undefined
#     {
#       port               = 80
#       protocol           = "HTTP"
#       target_group_index = 0
#       # action_type        = "forward"
#     },
#     {
#       port        = 81
#       protocol    = "HTTP"
#       action_type = "redirect"
#       redirect = {
#         port        = "443"
#         protocol    = "HTTPS"
#         status_code = "HTTP_301"
#       }
#     },
#     {
#       port        = 82
#       protocol    = "HTTP"
#       action_type = "fixed-response"
#       fixed_response = {
#         content_type = "text/plain"
#         message_body = "Fixed message"
#         status_code  = "200"
#       }
#     },
#   ]

#   https_listeners = [
#     {
#       port               = 443
#       protocol           = "HTTPS"
#       certificate_arn    = module.acm.this_acm_certificate_arn
#       target_group_index = 1
#     },
#     # Authentication actions only allowed with HTTPS
#     {
#       port               = 444
#       protocol           = "HTTPS"
#       action_type        = "authenticate-cognito"
#       target_group_index = 1
#       certificate_arn    = module.acm.this_acm_certificate_arn
#       authenticate_cognito = {
#         authentication_request_extra_params = {
#           display = "page"
#           prompt  = "login"
#         }
#         on_unauthenticated_request = "authenticate"
#         session_cookie_name        = "session-${random_pet.this.id}"
#         session_timeout            = 3600
#         user_pool_arn              = aws_cognito_user_pool.this.arn
#         user_pool_client_id        = aws_cognito_user_pool_client.this.id
#         user_pool_domain           = aws_cognito_user_pool_domain.this.domain
#       }
#     },
#     {
#       port               = 445
#       protocol           = "HTTPS"
#       action_type        = "authenticate-oidc"
#       target_group_index = 1
#       certificate_arn    = module.acm.this_acm_certificate_arn
#       authenticate_oidc = {
#         authentication_request_extra_params = {
#           display = "page"
#           prompt  = "login"
#         }
#         authorization_endpoint = "https://${local.domain_name}/auth"
#         client_id              = "client_id"
#         client_secret          = "client_secret"
#         issuer                 = "https://${local.domain_name}"
#         token_endpoint         = "https://${local.domain_name}/token"
#         user_info_endpoint     = "https://${local.domain_name}/user_info"
#       }
#     },
#   ]

#   https_listener_rules = [
#     {
#       https_listener_index = 0

#       actions = [
#         {
#           type = "authenticate-cognito"

#           on_unauthenticated_request = "authenticate"
#           session_cookie_name        = "session-${random_pet.this.id}"
#           session_timeout            = 3600
#           user_pool_arn              = aws_cognito_user_pool.this.arn
#           user_pool_client_id        = aws_cognito_user_pool_client.this.id
#           user_pool_domain           = aws_cognito_user_pool_domain.this.domain
#         },
#         {
#           type               = "forward"
#           target_group_index = 0
#         }
#       ]

#       conditions = [{
#         path_patterns = ["/some/auth/required/route"]
#       }]
#     },
#     {
#       https_listener_index = 1
#       priority             = 2

#       actions = [
#         {
#           type = "authenticate-oidc"

#           authentication_request_extra_params = {
#             display = "page"
#             prompt  = "login"
#           }
#           authorization_endpoint = "https://${local.domain_name}/auth"
#           client_id              = "client_id"
#           client_secret          = "client_secret"
#           issuer                 = "https://${local.domain_name}"
#           token_endpoint         = "https://${local.domain_name}/token"
#           user_info_endpoint     = "https://${local.domain_name}/user_info"
#         },
#         {
#           type               = "forward"
#           target_group_index = 1
#         }
#       ]

#       conditions = [{
#         host_headers = ["foobar.com"]
#       }]
#     },
#     {
#       https_listener_index = 0
#       priority             = 3
#       actions = [{
#         type         = "fixed-response"
#         content_type = "text/plain"
#         status_code  = 200
#         message_body = "This is a fixed response"
#       }]

#       conditions = [{
#         http_headers = [{
#           http_header_name = "x-Gimme-Fixed-Response"
#           values           = ["yes", "please", "right now"]
#         }]
#       }]
#     },
#     {
#       https_listener_index = 0
#       priority             = 5000
#       actions = [{
#         type        = "redirect"
#         status_code = "HTTP_302"
#         host        = "www.youtube.com"
#         path        = "/watch"
#         query       = "v=dQw4w9WgXcQ"
#         protocol    = "HTTPS"
#       }]

#       conditions = [{
#         query_strings = [{
#           key   = "video"
#           value = "random"
#         }]
#       }]
#     },
#   ]

#   target_groups = [
#     {
#       name_prefix          = "h1"
#       backend_protocol     = "HTTP"
#       backend_port         = 80
#       target_type          = "instance"
#       deregistration_delay = 10
#       health_check = {
#         enabled             = true
#         interval            = 30
#         path                = "/healthz"
#         port                = "traffic-port"
#         healthy_threshold   = 3
#         unhealthy_threshold = 3
#         timeout             = 6
#         protocol            = "HTTP"
#         matcher             = "200-399"
#       }
#       tags = {
#         InstanceTargetGroupTag = "baz"
#       }
#     },
#     {
#       name_prefix                        = "l1-"
#       target_type                        = "lambda"
#       lambda_multi_value_headers_enabled = true
#     },
#   ]

#   tags = {
#     Project = "Unknown"
#   }

#   lb_tags = {
#     MyLoadBalancer = "foo"
#   }

#   target_group_tags = {
#     MyGlobalTargetGroupTag = "bar"
#   }
# }



# ############################ APP ######################################

# module "task_elb" {
#   # Configure AWS Loadbalancer
#   source          = "./modules/AutoScaling/ELB"
#   create          = var.create
#   resource_create = var.task
#   name            = "task-elb-front"
#   subnets         = module.task_sub_public_devops.public_subnet_id
#   security_groups = module.task_elb_sg.sg_id
#   internal        = false

#   listener = [
#     {
#       instance_port     = "80"
#       instance_protocol = "HTTP"
#       lb_port           = "80"
#       lb_protocol       = "HTTP"
#     },
#   ]

#   health_check = {
#     target              = "HTTP:80/healthcheck/"
#     interval            = 30
#     healthy_threshold   = 2
#     unhealthy_threshold = 2
#     timeout             = 5
#   }

#   access_logs = {
#     bucket        = element(concat(module.task_databucket.bucket_id, list("")), 0)
#     bucket_prefix = "logs"
#   }

#   tags = var.tags
# }

# module "task_asg_launchconfig" {
#   # Configure AWS LaunchConfig
#   source                 = "./modules/AutoScaling/LaunchConfiguration"
#   module_depends_on      = [module.task_rds_posgresql]
#   create                 = var.create
#   resource_create        = var.task
#   ami                    = data.aws_ami.task_amazon-linux-2.id
#   name                   = "task-asg"
#   instance_type          = var.instance_type
#   iam_instance_profile   = join("", module.task_sg_devops_instance_profile.instance_profile_name)
#   key_name               = module.task_sg_devops_keypair.keypair_name
#   user_data              = try(data.template_file.bootstrap[0].rendered, null)
#   vpc_security_group_ids = module.task_sg_devops.sg_id

#   root_block_device = [
#     {
#       volume_type = "gp2"
#       volume_size = 10
#     },
#   ]

#   ebs_block_device = [
#     {
#       device_name = "/dev/sdf"
#       volume_type = "gp2"
#       volume_size = 5
#       encrypted   = true
#       kms_key_id  = data.aws_kms_alias.task_ebs.arn
#     }
#   ]
# }

# module "asg" {
#   source               = "./modules/AutoScaling/ASG"
#   create               = var.create
#   resource_create      = var.task
#   name                 = "task-devops-asg"
#   launch_configuration = module.task_asg_launchconfig.launchconfigurationname
#   vpc_zone_identifier  = module.task_sub_private_devops.private_subnet_id
#   max_size             = 3
#   min_size             = 2
#   desired_capacity     = 2
#   load_balancers       = module.task_elb.elb_name
#   tags_as_map          = var.asg_tags
#   health_check_type    = "EC2"
#   #force_delete = true
# }

# output "application_uri" {
#   description = "DNS endpoint to access the application"
#   value = format("%s%s", "http://", element(concat(module.task_elb.elb_dns_name, list("")), 0))
# }

// output "task_account_id" {
//   value = data.aws_caller_identity.task_current.account_id
// }

// output "task_caller_arn" {
//   value = data.aws_caller_identity.task_current.arn
// }

// output "task_caller_user" {
//   value = data.aws_caller_identity.task_current.user_id
// }

// output "task_availabeaznames" {
//   value = data.aws_availability_zones.task_availableaz.names
// }

// output "task_availabeazid" {
//   value = data.aws_availability_zones.task_availableaz.zone_ids
// }

// output "task_regionname" {
//   value = data.aws_region.task_current.name
// }

// output "task_vpc_id" {
//   value = module.task_vpc_devops.vpc_id
// }

// output "task_vpc_arn" {
//   value = module.task_vpc_devops.vpc_arn
// }

// output "task_vpc_cidr_block" {
//   value = module.task_vpc_devops.vpc_cidr_block
// }

// output "task_public_subnet_id" {
//   value = module.task_sub_public_devops.public_subnet_id
// }

// output "task_public_subnet_arn" {
//   value = module.task_sub_public_devops.public_subnet_arn
// }

// output "task_private_subnet_id" {
//   value = module.task_sub_private_devops.private_subnet_id
// }

// output "task_private_subnet_arn" {
//   value = module.task_sub_private_devops.private_subnet_arn
// }

// output "task_sg_devops_sgname" {
//   value = module.task_sg_devops.sg_name
// }

// output "task_sg_devops_sgarn" {
//   value = module.task_sg_devops.sg_arn
// }

// output "task_sg_devops_sgid" {
//   value = module.task_sg_devops.sg_id
// }

// output "task_sg_devops_publicrule_ingress_id" {
//   value = module.task_sg_devops_ingress.sg_rule_id
// }

// output "task_sg_devops_keypair_ssmarn" {
//   value = module.task_sg_devops_ssm.ssm_arn
// }
