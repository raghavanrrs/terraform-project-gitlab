#------------------Task 1 settings-----------------------------#
task                   = "true"
env                    = "dev"
region                 = "ap-southeast-1"
cidr                   = "10.0.0.0/16"
name                   = "vpc"
one_nat_gateway_per_az = "true"
public_subnets         = ["10.0.0.0/20", "10.0.16.0/20", "10.0.32.0/20"]
private_subnets        = ["10.0.48.0/20", "10.0.64.0/20", "10.0.80.0/20"]
database_subnets       = ["10.0.96.0/20", "10.0.112.0/20", "10.0.128.0/20"]
instance_type          = "t2.micro"
db_instance_class      = "db.t2.micro"
database_username      = "useradmin"
repo_url               = "https://github.com/servian/TechChallengeApp.git"