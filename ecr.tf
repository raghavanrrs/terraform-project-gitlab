# Allows specific accounts to pull images
module "ecr" {
    source = "./modules/ecr"
    scan_on_push         = true
    image_tag_mutability = "MUTABLE"
    attach_lifecycle_policy = true
    tags = var.tags
    # only_pull_accounts       = ["123456789012"] # data.aws_cloudtrail_service_account.main.id
    # push_and_pull_accounts   = ["111111111111"] # data.aws_cloudtrail_service_account.main.id
    max_untagged_image_count = 5
    max_tagged_image_count   = 50

    # Note that currently only one policy may be applied to a repository.
    #policy = <<EOF
    # {
    #     "Version": "2008-10-17",
    #     "Statement": [
    #         {
    #             "Sid": "repo policy",
    #             "Effect": "Allow",
    #             "Principal": "*",
    #             "Action": [
    #                 "ecr:GetDownloadUrlForLayer",
    #                 "ecr:BatchGetImage",
    #                 "ecr:BatchCheckLayerAvailability",
    #                 "ecr:PutImage",
    #                 "ecr:InitiateLayerUpload",
    #                 "ecr:UploadLayerPart",
    #                 "ecr:CompleteLayerUpload",
    #                 "ecr:DescribeRepositories",
    #                 "ecr:GetRepositoryPolicy",
    #                 "ecr:ListImages",
    #                 "ecr:DeleteRepository",
    #                 "ecr:BatchDeleteImage",
    #                 "ecr:SetRepositoryPolicy",
    #                 "ecr:DeleteRepositoryPolicy"
    #             ]
    #         }
    #     ]
    # }
    # EOF
}