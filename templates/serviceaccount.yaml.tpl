apiVersion: v1
kind: ServiceAccount
metadata:
  name: ${service_account_name}
  namespace: default
  annotations:
    eks.amazonaws.com/role-arn: ${app_iam_role_arn}
