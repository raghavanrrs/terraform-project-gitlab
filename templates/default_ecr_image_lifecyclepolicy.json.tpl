{
    "rules": [
        {
        rulePriority = 1
        description  = "Keep last \"${var.max_untagged_image_count}\" untagged images"
        selection = {
          tagStatus   = "untagged"
          countType   = "imageCountMoreThan"
          countNumber = "${var.max_untagged_image_count}"
        }
        action = {
          type = "expire"
        }
      },
      {
        rulePriority = 2
        description  = "Keep last \"${var.max_tagged_image_count}\" tagged images"
        selection = {
          tagStatus     = "tagged"
          tagPrefixList = "${var.tag_prefix_list}"
          countType     = "imageCountMoreThan"
          countNumber   = "${var.max_tagged_image_count}"
        }
        action = {
          type = "expire"
        }
      },
    ]
}