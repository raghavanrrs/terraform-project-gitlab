import json

"""Extract nested values from a JSON tree."""


def json_extract(obj, key):
    """Recursively fetch values from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    values = extract(obj, arr, key)
    return values


with open("tfplan.json") as state_data:
     data = json.load(state_data)
address = json_extract(data, 'address')
mode = json_extract(data, "planned_values")
print(address)
print(mode)
# print(address)
# print(mode)
#print("together: {}".format(dict(zip(address, mode))))
# for k, v in zip(address, mode):
#     print(k, ":", v)
