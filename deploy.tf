module "pipeline-artifact-bucket" {
  source                  = "./modules/s3"
  bucket_prefix           = "artifactbucket"
  s3_bucket_force_destroy = true
  create                  = var.create
  resource_create         = var.task
  kms_arn                 = data.aws_kms_alias.s3.arn
  tags                    = var.tags
  sse_algorithm           = "AES256"
  policy                  = <<EOF
{
  "Version": "2012-10-17",
  "Id": "PIPELINEARTIFACTBUCKETPOLICY",
  "Statement": [
    {
      "Sid": "DenyUnEncryptedObjectUploads",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${var.create && var.task && length(module.pipeline-artifact-bucket.bucket_id) > 0 ? element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0) : "*"}/*",
      "Condition": {
          "StringNotEquals": {
              "s3:x-amz-server-side-encryption": "aws:kms"
          }
      }
    },
    {
      "Sid": "DenyInsecureConnections",
      "Effect": "Deny",
      "Principal": "*",
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::${var.create && var.task && length(module.pipeline-artifact-bucket.bucket_id) > 0 ? element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0) : "*"}/*",
      "Condition": {
          "Bool": {
              "aws:SecureTransport": "false"
          }
        }
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "assume_by_pipeline" {
  statement {
    sid     = "AllowAssumeByPipeline"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

module "pipeline_role" {
  source             = "./modules/iam/service_role"
  create             = var.create
  resource_create    = var.task
  name               = "code-pipeline-role"
  tags               = var.tags
  assume_role_policy = data.aws_iam_policy_document.assume_by_pipeline.json
}

data "aws_iam_policy_document" "pipeline" {
  statement {
    sid    = "AllowS3"
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${var.create && var.task && length(module.pipeline-artifact-bucket.bucket_id) > 0 ? element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0) : "*"}/*",
      "arn:aws:s3:::${var.create && var.task && length(module.pipeline-artifact-bucket.bucket_id) > 0 ? element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0) : "*"}",
    ]
  }

  statement {
    sid    = "AllowCodeBuild"
    effect = "Allow"

    actions = [
      "codebuild:BatchGetBuilds",
      "codebuild:StartBuild",
    ]

    resources = [element(concat(module.codebuild_terraform_plan.codebuild_arn, list("")), 0), element(concat(module.codebuild_terraform_apply.codebuild_arn, list("")), 0)]
  }

  statement {
    sid    = "AllowCodeDeploy"
    effect = "Allow"

    actions = [
      "codedeploy:CreateDeployment",
      "codedeploy:GetApplication",
      "codedeploy:GetApplicationRevision",
      "codedeploy:GetDeployment",
      "codedeploy:GetDeploymentConfig",
      "codedeploy:RegisterApplicationRevision",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "AllowECS"
    effect = "Allow"

    actions = ["ecs:*"]

    resources = ["*"]
  }

  statement {
    sid    = "AllowPassRole"
    effect = "Allow"

    resources = ["*"]

    actions = ["iam:PassRole"]

    condition {
      test     = "StringLike"
      values   = ["ecs-tasks.amazonaws.com"]
      variable = "iam:PassedToService"
    }
  }
}

module "pipeline_role_policy" {
  source          = "./modules/iam/policy"
  create          = var.create
  resource_create = var.task
  name            = "code-pipeline-role-policy"
  path            = "/"
  description     = "My pipeline role policy"

  policy = data.aws_iam_policy_document.pipeline.json
}


module "pipeline_role_policy_attachment" {
  source          = "./modules/iam/policy_attachment"
  create          = var.create
  resource_create = var.task
  roles           = module.pipeline_role.role_name
  policy_arn      = module.pipeline_role_policy.arn
}

# data "aws_iam_policy_document" "assume_by_codedeploy" {
#   statement {
#     sid     = ""
#     effect  = "Allow"
#     actions = ["sts:AssumeRole"]

#     principals {
#       type        = "Service"
#       identifiers = ["codedeploy.amazonaws.com"]
#     }
#   }
# }

# module "codedeploy_role" {
#   source             = "./modules/iam/service_role"
#   create             = var.create
#   resource_create    = var.task
#   name               = "codedeploy_role"
#   tags               = var.tags
#   assume_role_policy = data.aws_iam_policy_document.assume_by_codedeploy.json
# }

# data "aws_iam_policy_document" "codedeploy" {
#   statement {
#     sid    = "AllowLoadBalancingAndECSModifications"
#     effect = "Allow"

#     actions = [
#       "ecs:CreateTaskSet",
#       "ecs:DeleteTaskSet",
#       "ecs:DescribeServices",
#       "ecs:UpdateServicePrimaryTaskSet",
#       "elasticloadbalancing:DescribeListeners",
#       "elasticloadbalancing:DescribeRules",
#       "elasticloadbalancing:DescribeTargetGroups",
#       "elasticloadbalancing:ModifyListener",
#       "elasticloadbalancing:ModifyRule",
#     ]

#     resources = ["*"]
#   }

#   statement {
#     sid    = "AllowS3"
#     effect = "Allow"

#     actions = ["s3:GetObject"]

#     resources = ["arn:aws:s3:::${var.create && var.task && length(module.pipeline-artifact-bucket.bucket_id) > 0 ? element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0) : "*"}/*",
#       "arn:aws:s3:::${var.create && var.task && length(module.pipeline-artifact-bucket.bucket_id) > 0 ? element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0) : "*"}",]
#   }

#   statement {
#     sid    = "AllowPassRole"
#     effect = "Allow"

#     actions = ["iam:PassRole"]

#     resources = [
#       "aws_iam_role.execution_role.arn",
#       "aws_iam_role.task_role.arn",
#     ]
#   }
# }

# module "codedeploy_role_policy" {
#   source          = "./modules/iam/policy"
#   create          = var.create
#   resource_create = var.task
#   name            = "codedeploy-role-policy"
#   path            = "/"
#   description     = "My codedeploy role policy"

#   policy = data.aws_iam_policy_document.codedeploy.json
# }

# module "codedeploy_role_policy_attachment" {
#   source          = "./modules/iam/policy_attachment"
#   create          = var.create
#   resource_create = var.task
#   roles           = module.codedeploy_role.role_name
#   policy_arn      = module.codedeploy_role_policy.arn
# }

# resource "aws_codebuild_source_credential" "authorization" {
#   count       = var.enabled && var.private_repository ? 1 : 0
#   auth_type   = var.source_credential_auth_type
#   server_type = var.source_credential_server_type
#   token       = var.source_credential_token
#   user_name   = var.source_credential_user_name
# }
####### Codebuild resource block ###################

data "aws_iam_policy_document" "assume_by_codebuild" {
  statement {
    sid     = "AllowAssumeByCodebuild"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

module "codebuild_role" {
  source             = "./modules/iam/service_role"
  create             = var.create
  resource_create    = var.task
  name               = "codebuild-role"
  tags               = var.tags
  assume_role_policy = data.aws_iam_policy_document.assume_by_codebuild.json
}

data "aws_iam_policy_document" "codebuild" {
  statement {
    sid    = "AllowS3"
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:GetBucketVersioning",
      "s3:GetObjectVersion",
      "s3:ListBucket",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${var.create && var.task && length(module.pipeline-artifact-bucket.bucket_id) > 0 ? element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0) : "*"}/*",
      "arn:aws:s3:::${var.create && var.task && length(module.pipeline-artifact-bucket.bucket_id) > 0 ? element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0) : "*"}",
    ]
  }

  statement {
    sid    = "AllowECRAuth"
    effect = "Allow"

    actions = ["ecr:GetAuthorizationToken"]

    resources = ["*"]
  }

  statement {
    sid    = "AllowEC2Describe"
    effect = "Allow"

    actions = ["ec2:Describe*"]

    resources = ["*"]
  }

  statement {
    sid    = "AllowECRUpload"
    effect = "Allow"

    actions = [
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload",
      "ecr:BatchCheckLayerAvailability",
      "ecr:PutImage",
    ]

    #resources = [data.aws_ecr_repository.this.arn]
    resources = ["*"]
  }

  statement {
    sid       = "AllowECSDescribeTaskDefinition"
    effect    = "Allow"
    actions   = ["ecs:DescribeTaskDefinition"]
    resources = ["*"]
  }

  statement {
    sid    = "AllowLogging"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = [
      "arn:aws:logs:${var.region}:${data.aws_caller_identity.task_current.account_id}:log-group:/aws/codebuild/codebuild*",
      "arn:aws:logs:${var.region}:${data.aws_caller_identity.task_current.account_id}:log-group:/aws/codebuild",
    ]
  }
}

module "codebuild_role_policy" {
  source          = "./modules/iam/policy"
  create          = var.create
  resource_create = var.task
  name            = "codebuild-role-policy"
  path            = "/service-role/"
  description     = "My codebuild role policy"

  policy = data.aws_iam_policy_document.codebuild.json
}

module "codebuild_role_policy_attachment" {
  source          = "./modules/iam/policy_attachment"
  create          = var.create
  resource_create = var.task
  roles           = module.codebuild_role.role_name
  policy_arn      = module.codebuild_role_policy.arn
}

data "aws_iam_policy_document" "codebuild_default_kms" {
  version = "2012-10-17"
  statement {
    sid    = "EnableIAMUserPermissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.task_current.account_id}:root",
        data.aws_caller_identity.task_current.arn,
      ]
    }
    actions = ["kms:*"]
    resources = ["*"]
  }
  statement {
    sid    = "AllowCloudTrailEncryptLogs"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["kms:GenerateDataKey*"]
    resources = ["*"]
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
      values   = ["arn:aws:cloudtrail:*:${data.aws_caller_identity.task_current.account_id}:trail/*"]
    }
  }

  statement {
    sid    = "AllowCloudTrailDescribeKey"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["kms:DescribeKey"]
    resources = ["*"]
  }

  statement {
    sid    = "AllowPrincipalsAccountDecryptLogFiles"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions = [
      "kms:Decrypt",
      "kms:ReEncryptFrom"
    ]
    resources = ["*"]
    condition {
      test     = "StringEquals"
      variable = "kms:CallerAccount"
      values = [
        "${data.aws_caller_identity.task_current.account_id}"
      ]
    }
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
      values   = ["arn:aws:cloudtrail:*:${data.aws_caller_identity.task_current.account_id}:trail/*"]
    }
  }

  statement {
    sid    = "AllowAliasCreation"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions   = ["kms:CreateAlias"]
    resources = ["*"]
  }
}

# module "codebuild_role_kms_policy" {
#   source          = "./modules/iam/policy"
#   create          = var.create
#   resource_create = var.task
#   name            = "codebuild-role-kms-policy"
#   path            = "/"
#   description     = "Codebuild role kms policy"

#   policy = data.aws_iam_policy_document.codebuild_default_kms.json
# }

# module "codebuild_role_kms_policy_attachment" {
#   source          = "./modules/iam/policy_attachment"
#   create          = var.create
#   resource_create = var.task
#   roles           = module.codebuild_role.role_name
#   policy_arn      = module.codebuild_role_kms_policy.arn
# }

module "codebuild_cmk_kms" {
  source          = "./modules/kms"
  create          = var.create
  resource_create = var.task
  app             = "codebuild"
  env             = terraform.workspace
  description     = "CMK for codebuild"
  #policy          = data.aws_iam_policy_document.codebuild_default_kms.json
}

module "codebuild_terraform_plan" {
  source               = "./modules/codebuild"
  create               = var.create
  resource_create      = var.task
  app                  = "terraform-plan"
  env                  = terraform.workspace
  description          = "Codebuild for Terraform Plan"
  service_role         = module.codebuild_role.role_arn
  encryption_key       = module.codebuild_cmk_kms.keyarn
  #account_id           = data.aws_caller_identity.task_current.account_id
  report_build_status  = true
  default_log_location = module.pipeline-artifact-bucket.bucket_id
  environment = {
    compute_type    = "BUILD_GENERAL1_SMALL"
    image           = "aws/codebuild/standard:2.0"
    type            = "LINUX_CONTAINER"
    privileged_mode = true
    #Environment variables
    environment_variables = [
      {
        name  = "STAGE"
        value = "plan"
      },
    ]
  }
  # artifacts
  artifacts = {
    type                = "CODEPIPELINE"
    encryption_disabled = false
  }
  # Cache
  cache = {
    type     = "S3"
    location = element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0)
  }
  # Logs
  logs_config = {
    s3_logs = {
      status   = "ENABLED"
      location = "${element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0)}/plan-log"
    }
  }
  # Tags
  tags = {
    Environment = terraform.workspace
    owner       = "ccoe"
    stage       = "plan"
  }
  # # VPC
  # vpc_config = {
  #   vpc_id             = "${element(concat(module.task_vpc_devops.vpc_id, list("")), 0)}"
  #   subnets            = module.task_sub_private_devops.private_subnet_id
  #   security_group_ids = module.task_elb_sg.sg_id
  # }
}

module "codebuild_terraform_apply" {
  source               = "./modules/codebuild"
  create               = var.create
  resource_create      = var.task
  app                  = "terraform-apply"
  env                  = terraform.workspace
  description          = "Codebuild for Terraform Apply"
  service_role         = module.codebuild_role.role_arn
  encryption_key       = module.codebuild_cmk_kms.keyarn
  #report_build_status  = true
  default_log_location = module.pipeline-artifact-bucket.bucket_id
  environment = {
    compute_type    = "BUILD_GENERAL1_SMALL"
    image           = "aws/codebuild/standard:2.0"
    type            = "LINUX_CONTAINER"
    privileged_mode = true

    # Environment variables
    environment_variables = [
      {
        name  = "STAGE"
        value = "apply"
      },
    ]
  }
  # artifacts
  artifacts = {
    type                = "CODEPIPELINE"
    encryption_disabled = false
  }
  # Cache
  cache = {
    type     = "S3"
    location = element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0)
  }
  # Logs
  logs_config = {
    s3_logs = {
      status   = "ENABLED"
      location = "${element(concat(module.pipeline-artifact-bucket.bucket_id, list("")), 0)}/apply-log"
    }
  }
  # Tags
  tags = {
    Environment = terraform.workspace
    owner       = "ccoe"
    stage       = "apply"
  }
  # VPC
  # vpc_config = {
  #   vpc_id             = "${element(concat(module.task_vpc_devops.vpc_id, list("")), 0)}"
  #   subnets            = module.task_sub_private_devops.private_subnet_id
  #   security_group_ids = module.task_elb_sg.sg_id
  # }
}

# data "template_file" "container_definition" {
#   template = file("${path.module}/templates/container-definition.json.tpl")

#   vars = {
#     command                = local.command == "[]" ? "null" : local.command
#     cpu                    = var.cpu == 0 ? "null" : var.cpu
#     disableNetworking      = var.disableNetworking ? true : false
#     dnsSearchDomains       = local.dnsSearchDomains == "[]" ? "null" : local.dnsSearchDomains
#     dnsServers             = local.dnsServers == "[]" ? "null" : local.dnsServers
#     dockerLabels           = local.dockerLabels == "{}" ? "null" : local.dockerLabels
#     dockerSecurityOptions  = local.dockerSecurityOptions == "[]" ? "null" : local.dockerSecurityOptions
#     entryPoint             = local.entryPoint == "[]" ? "null" : local.entryPoint
#     environment            = local.environment == "[]" ? "null" : local.environment
#     essential              = var.essential ? true : false
#     extraHosts             = local.extraHosts == "[]" ? "null" : local.extraHosts
#     healthCheck            = local.healthCheck == "{}" ? "null" : local.healthCheck
#     hostname               = var.hostname == "" ? "null" : var.hostname
#     image                  = var.image == "" ? "null" : var.image
#     interactive            = var.interactive ? true : false
#     links                  = local.links == "[]" ? "null" : local.links
#     linuxParameters        = local.linuxParameters == "{}" ? "null" : local.linuxParameters
#     logConfiguration       = local.logConfiguration == "{}" ? "null" : local.logConfiguration
#     memory                 = var.memory == 0 ? "null" : var.memory
#     memoryReservation      = var.memoryReservation == 0 ? "null" : var.memoryReservation
#     mountPoints            = local.mountPoints == "[]" ? "null" : local.mountPoints
#     name                   = var.name == "" ? "null" : var.name
#     portMappings           = local.portMappings == "[]" ? "null" : local.portMappings
#     privileged             = var.privileged ? true : false
#     pseudoTerminal         = var.pseudoTerminal ? true : false
#     readonlyRootFilesystem = var.readonlyRootFilesystem ? true : false
#     repositoryCredentials  = local.repositoryCredentials == "{}" ? "null" : local.repositoryCredentials
#     resourceRequirements   = local.resourceRequirements == "[]" ? "null" : local.resourceRequirements
#     secrets                = local.secrets == "[]" ? "null" : local.secrets
#     systemControls         = local.systemControls == "[]" ? "null" : local.systemControls
#     ulimits                = local.ulimits == "[]" ? "null" : local.ulimits
#     user                   = var.user == "" ? "null" : var.user
#     volumesFrom            = local.volumesFrom == "[]" ? "null" : local.volumesFrom
#     workingDirectory       = var.workingDirectory == "" ? "null" : var.workingDirectory
#   }
# }

# resource "aws_ecs_task_definition" "ecs_task_definition" {
#   container_definitions = local.container_definitions
#   execution_role_arn    = var.execution_role_arn
#   family                = var.family
#   ipc_mode              = var.ipc_mode
#   network_mode          = var.network_mode
#   pid_mode              = var.pid_mode

#   # Fargate requires cpu and memory to be defined at the task level
#   cpu    = var.cpu
#   memory = var.memory

#   dynamic "placement_constraints" {
#     for_each = var.placement_constraints
#     content {
#       # TF-UPGRADE-TODO: The automatic upgrade tool can't predict
#       # which keys might be set in maps assigned here, so it has
#       # produced a comprehensive set here. Consider simplifying
#       # this after confirming which keys can be set in practice.

#       expression = lookup(placement_constraints.value, "expression", null)
#       type       = placement_constraints.value.type
#     }
#   }
#   requires_compatibilities = var.requires_compatibilities
#   task_role_arn            = var.task_role_arn
#   dynamic "volume" {
#     for_each = var.volumes
#     content {
#       # TF-UPGRADE-TODO: The automatic upgrade tool can't predict
#       # which keys might be set in maps assigned here, so it has
#       # produced a comprehensive set here. Consider simplifying
#       # this after confirming which keys can be set in practice.

#       host_path = lookup(volume.value, "host_path", null)
#       name      = volume.value.name

#       dynamic "docker_volume_configuration" {
#         for_each = lookup(volume.value, "docker_volume_configuration", [])
#         content {
#           autoprovision = lookup(docker_volume_configuration.value, "autoprovision", null)
#           driver        = lookup(docker_volume_configuration.value, "driver", null)
#           driver_opts   = lookup(docker_volume_configuration.value, "driver_opts", null)
#           labels        = lookup(docker_volume_configuration.value, "labels", null)
#           scope         = lookup(docker_volume_configuration.value, "scope", null)
#         }
#       }
#       dynamic "efs_volume_configuration" {
#         for_each = lookup(volume.value, "efs_volume_configuration", [])
#         content {
#           file_system_id = lookup(efs_volume_configuration.value, "file_system_id", null)
#           root_directory = lookup(efs_volume_configuration.value, "root_directory", null)
#         }
#       }
#     }
#   }

#   count = var.register_task_definition ? 1 : 0
# }

# locals {
#   command               = jsonencode(var.command)
#   dnsSearchDomains      = jsonencode(var.dnsSearchDomains)
#   dnsServers            = jsonencode(var.dnsServers)
#   dockerLabels          = jsonencode(var.dockerLabels)
#   dockerSecurityOptions = jsonencode(var.dockerSecurityOptions)
#   entryPoint            = jsonencode(var.entryPoint)
#   environment           = jsonencode(var.environment)
#   extraHosts            = jsonencode(var.extraHosts)

#   healthCheck = replace(jsonencode(var.healthCheck), local.classes["digit"], "$1")

#   links = jsonencode(var.links)

#   linuxParameters = replace(
#     replace(
#       replace(jsonencode(var.linuxParameters), "/\"1\"/", "true"),
#       "/\"0\"/",
#       "false",
#     ),
#     local.classes["digit"],
#     "$1",
#   )

#   logConfiguration = jsonencode(var.logConfiguration)

#   mountPoints = replace(
#     replace(jsonencode(var.mountPoints), "/\"1\"/", "true"),
#     "/\"0\"/",
#     "false",
#   )

#   portMappings = replace(jsonencode(var.portMappings), local.classes["digit"], "$1")

#   repositoryCredentials = jsonencode(var.repositoryCredentials)
#   resourceRequirements  = jsonencode(var.resourceRequirements)
#   secrets               = jsonencode(var.secrets)
#   systemControls        = jsonencode(var.systemControls)

#   ulimits = replace(jsonencode(var.ulimits), local.classes["digit"], "$1")

#   volumesFrom = replace(
#     replace(jsonencode(var.volumesFrom), "/\"1\"/", "true"),
#     "/\"0\"/",
#     "false",
#   )

#   # re2 ASCII character classes
#   # https://github.com/google/re2/wiki/Syntax
#   classes = {
#     digit = "/\"(-[[:digit:]]|[[:digit:]]+)\"/"
#   }

#   container_definition = var.register_task_definition ? format("[%s]", data.template_file.container_definition.rendered) : format("%s", data.template_file.container_definition.rendered)

#   container_definitions = replace(local.container_definition, "/\"(null)\"/", "$1")
# }

# module "wordpress" {
#   source = "mongodb/ecs-task-definition/aws"

#   name = "wordpress"

#   links = [
#     "mysql",
#   ]

#   image     = "wordpress"
#   essential = true

#   portMappings = [
#     {
#       containerPort = 80
#       hostPort      = 80
#     },
#   ]

#   memory = 500
#   cpu    = 10

#   register_task_definition = false
# }

# module "mysql" {
#   source = "mongodb/ecs-task-definition/aws"

#   environment = [
#     {
#       name  = "MYSQL_ROOT_PASSWORD"
#       value = "password"
#     },
#   ]

#   name      = "mysql"
#   image     = "mysql"
#   cpu       = 10
#   memory    = 500
#   essential = true

#   register_task_definition = false
# }

# module "merged" {
#   source = "mongodb/ecs-task-definition/aws//modules/merge"

#   container_definitions = [
#     "${module.wordpress.container_definitions}",
#     "${module.mysql.container_definitions}",
#   ]
# }

# resource "aws_ecs_task_definition" "hello_world" {
#   container_definitions = "${module.merged.container_definitions}"
#   family                = "hello_world"
# }

# variable "container_definitions" {
#   default     = []
#   description = "A list of container definitions in JSON format that describe the different containers that make up your task"
# }

# output "container_definitions" {
#   description = "A list of container definitions in JSON format that describe the different containers that make up your task"
#   value       = "${format("[%s]", join(",", var.container_definitions))}"
# }