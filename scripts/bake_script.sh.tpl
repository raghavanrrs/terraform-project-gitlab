#!/bin/bash
set -e
sudo amazon-linux-extras install docker -y
sudo yum install dos2unix git jq nc -y
dos2unix $0
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo chkconfig docker on
sudo amazon-linux-extras install postgresql10 -y
date_format=$(date +%F-%H-%M)
APP_DIR="/opt/app"
HOME_DIR="$APP_DIR/TechChallengeApp"
LOG_FILE="/var/log/TechChallengeApp.log"
APP_COMMIT_ID=${commit_id}
SECRET_ID=${secret_id}
APP_PORT=${app_port}
REPO_URL=${repo_url}
EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
EC2_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed 's/[a-z]$//'`"
RDS_HOSTNAME=${rds_hostname}
DB_NAME=${db_name}
DB_USER=${rds_username}
ENDPOINT=`echo $RDS_HOSTNAME | cut -f1 -d:`
DB_PORT=`echo $RDS_HOSTNAME | cut -f2 -d:`
echo -e "---------------------------- $(date +%F-%H) ----------------------------------" >> $LOG_FILE
echo -e "##############################################################################" >> $LOG_FILE
echo -e "#                 User-data Execution Started at $(date +%F-%H-%M)           #" >> $LOG_FILE
echo -e "##############################################################################" >> $LOG_FILE
echo -e "REPO_URL: $REPO_URL" >> $LOG_FILE
echo -e "APP_PORT: $APP_PORT" >> $LOG_FILE
echo -e "EC2_AVAIL_ZONE: $EC2_AVAIL_ZONE" >> $LOG_FILE
echo -e "EC2_REGION: $EC2_REGION" >> $LOG_FILE
echo -e "Building on commit id: $APP_COMMIT_ID" >> $LOG_FILE
mkdir $APP_DIR && cd $APP_DIR
echo -e "Cloning repo to $APP_DIR..."
git clone $REPO_URL
cd $HOME_DIR
DB_SECRET=`aws secretsmanager get-secret-value --secret-id $SECRET_ID --region $EC2_REGION | jq --raw-output '.SecretString'`
echo -e "Setting up values in conf.toml.."
sed -i -e "/DbHost/s/localhost/$ENDPOINT/" -e "/DbPort/s/5432/$DB_PORT/" -e "/ListenHost/s/localhost/0.0.0.0/" -e "/DbPassword/s/changeme/$DB_SECRET/" -e "/DbName/s/app/$DB_NAME/" -e "/ListenPort/s/3000/$APP_PORT/" -e "/DbUser/s/postgres/$DB_USER/" conf.toml
echo "Building image for app"
docker build . -t app-$date_format >> $LOG_FILE
container_id=$(docker run -d -it -p 80:$APP_PORT --name go-app-$date_format app-$date_format serve)
echo -e "$(date +%F-%H-%M) --> Container started: $container_id" >> $LOG_FILE
sleep 10
response=`curl --max-time 5 localhost/healthcheck/`
if [[ $response != 'OK' && $response = 'Error: db connection down' ]] # Check for application health
then
echo "Checking DB connectivity.." >> $LOG_FILE
if nc -z $ENDPOINT $DB_PORT # Check for DB connectivity
then
echo "$(date +%F-%H-%M) --> DB is connected..running updatedb command" >> $LOG_FILE
docker run --rm app-$date_format updatedb -s
if [ $? -eq 0 ] # check for table creation
then
echo -e "$(date +%F-%H-%M) --> DB table created and data seeded" >> $LOG_FILE
else
echo -e "$(date +%F-%H-%M) --> DB table not created" >> $LOG_FILE
fi
else
echo "$(date +%F-%H-%M) --> DB is not connected.." >> $LOG_FILE
fi
else
echo "$(date +%F-%H-%M) --> App is up and running" >> $LOG_FILE	
fi
DB_SECRET='' # reset db params
RDS_HOSTNAME='' # reset db params
echo -e "##############################################################################" >> $LOG_FILE
echo -e "#                 User-data Execution Completed at $(date +%F-%H-%M)         #" >> $LOG_FILE
echo -e "##############################################################################" >> $LOG_FILE
echo -e "\n" >> $LOG_FILE