variable "region" {
  default = "ap-southeast-1"
}

variable "create" {
  description = "A master control variable to control resource creation"
  default     = "false"
}

variable "tags" {
  type = map
  default = {
    "Terraform" : "true"
  }
}

variable "asg_tags" {
  type = map(string)
  default = {
    "Terraform" = "true"
  }
}

variable "state_bucket_prefix" {
  description = "Creates a unique state bucket name beginning with the specified prefix."
  default     = "tf-remote-state"
}

variable "state_bucket_force_destroy" {
  description = "A boolean that indicates all objects should be deleted from S3 buckets so that the buckets can be destroyed without error. These objects are not recoverable."
  default     = false
}

variable "env" {
  default = "dev"
}

variable "cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR"
  type        = string
  default     = "10.0.0.0/16"
}

variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = "dev"
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  type        = bool
  default     = false
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  type        = bool
  default     = true
}

variable "single_nat_gateway" {
  type    = bool
  default = false
}

variable "one_nat_gateway_per_az" {
  type    = bool
  default = false
}

variable "public_subnets" {
  type    = list(string)
  default = []
}

variable "private_subnets" {
  type    = list(string)
  default = []
}

variable "database_subnets" {
  type    = list(string)
  default = []
}

variable "public_ingress_rules" {
  description = "Map of known security group rules (define as 'name' = ['from port', 'to port', 'protocol', 'description'])"
  type        = list(string)
  default     = ["http-80-tcp", "ssh-tcp"]
}

variable "mysql_ingress_rules" {
  description = "Map of known security group rules (define as 'name' = ['from port', 'to port', 'protocol', 'description'])"
  type        = list(string)
  default     = ["mysql-tcp"]
}

variable "postgres_ingress_rules" {
  description = "Map of known security group rules (define as 'name' = ['from port', 'to port', 'protocol', 'description'])"
  type        = list(string)
  default     = ["postgres-tcp"]
}

variable "instance_count" {
  description = "Number of instances to launch"
  type        = number
  default     = 0
}

variable "instance_type" {
  description = "Instance type"
  default     = "t2.micro"
}

variable "upload_directory" {
  default = "path.module"
}

variable "create_ngw" {
  type    = bool
  default = false
}

variable "task" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  type        = bool
  default     = "false"
}

variable "db_instance_class" {
  default = "db.t2.micro"
}

variable "database_username" {
  default = "useradmin"
}

variable "shell_script" {
  description = "A shell script template that will be run from the ephemeral instanceRDS database name"
  default     = ""
}

variable "sql_script" {
  description = "A SQL script that will be run from the ephemeral instance against a MySQL/Aurora RDS DB"
  default     = ""
}

variable "green" {
  type        = bool
  description = "(optional) describe your green environment"
  default     = false
}

variable "application_port" {
  type    = number
  default = "3000"
}

variable "repo_url" {
  type        = string
  description = "(optional) describe your variable"
  default     = "https://github.com/servian/TechChallengeApp.git"
}

# variable "app_commit_id" {
#   type = string
#   description = "Application Commit ID from the upstream job"
# }