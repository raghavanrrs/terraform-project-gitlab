# terraform-project-gitlab

Prerequisites:

1. gitlab repo(one for app code and one for IAC)
2. AWS account
3. IAM user with ADMIN access
4. Access Key and Secret Key
5. S3 bucket and DynamoDB for remote state management

Steps to recreate deployment:

1. Login to gitlab.com

2. Create a new Project and navigate to Setting and click `Expand` under Variables option and start creating the following variables for our pipeline.
    a. AWS_ACCESS_KEY_ID       --> aws access key (masked)
    b. AWS_DEFAULT_REGION      --> aws region
    c. AWS_DYNAMODB_STATE      --> dynamodb table name, (eg: gitlab-ci-demo-backend)
    d. AWS_KMS_KEY_ID          --> aws/s3 kms (masked)
    e. AWS_REMOTESTATE_KEY     --> terraform
    f. AWS_SECRET_ACCESS_KEY   --> aws secret key (masked)
    g. AWS_STATE_BUCKET        --> state bucket name (eg: gitlab-ci-demo-backend)
    
3. Map your `.gitlab-ci.yml`(Assuming its kept under root dir) under Settings --> CI/CD --> General pipelines for enabling trigger.

4. Expand `Pipeline triggers` to create an automatic trigger for deployment by the app repo

5. Create S3 bucket with the below policy
{
    "Version": "2012-10-17",
    "Id": "RequireEncryption",
    "Statement": [
        {
            "Sid": "RequireEncryptedTransport",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::<bucket_name>/*",
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        },
        {
            "Sid": "RequireEncryptedStorage",
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::<bucket_name>/*",
            "Condition": {
                "StringNotEquals": {
                    "s3:x-amz-server-side-encryption": "AES256"
                }
            }
        }
    ]
}

6. Create dynamo db as per the link: https://www.terraform.io/docs/backends/types/s3.html
    create table with `LockID` of type string as Primary key

7. Edit master.tfvars and change the value `create` from `false` to `true`

8. Edit task.tfvars and change the value `task` from `false` to `true`

9. Commit and Push the changes to repo and you can view CI under CI/CD --> pipelines

10. The step `TF_APPLY` is set to manual to control the deployment.

11. Once approved the setup is complete.

# Follow docs/Solution.docx for detailed steps and architecture